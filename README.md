## About

SPA in Elm 0.19, using web push notifications. Application offers fundraising for small projects. 

![Projects](https://i.imgur.com/fZFXFqzl.png) ![Preview](https://i.imgur.com/w17zv9sl.png)