module Api exposing
    ( Kind(..)
    , Request
    , call
    )

import Api.Call.Procedure
import Api.Call.Rest
import Api.Interface
import Component.Credentials exposing (Credentials)
import Http exposing (Response)
import Json.Decode
import Url


type Kind a
    = Procedure (Api.Call.Procedure.Request a)
    | Rest (Api.Call.Rest.Request a)


type alias Request a msg =
    { kind : Kind a
    , change : Result String a -> msg
    , timeout : Maybe Float
    , tracker : Maybe String
    }


call : { session | url : Url.Url, credentials : Maybe Credentials } -> Request a msg -> Cmd msg
call s r =
    let
        p =
            prepare r.kind

        url =
            { protocol = s.url.protocol
            , host = s.url.host
            , port_ = s.url.port_
            , path = "/" ++ String.join "/" ([ Api.Interface.prefix, Api.Interface.version, path r.kind ] ++ p.path) ++ "/"
            , query = p.query
            , fragment = Nothing
            }
    in
    Http.request
        { method = p.method
        , headers = headers s.credentials
        , url = Url.toString url
        , body = p.body
        , expect = expect r.change p.decoder
        , timeout = r.timeout
        , tracker = r.tracker
        }

headers : Maybe Credentials -> List Http.Header
headers c =
    case c of
        Just c_ ->
            [ Http.header "Authorization" ("Basic " ++ c_.hash)
            ]

        Nothing ->
            []

prepare : Kind a -> Api.Interface.Prepare a
prepare k =
    case k of
        Procedure k_ ->
            Api.Call.Procedure.prepare k_

        Rest k_ ->
            Api.Call.Rest.prepare k_

path : Kind a -> String
path k =
    case k of
        Procedure _ ->
            Api.Call.Procedure.path

        Rest _ ->
            Api.Call.Rest.path


expect : (Result String a -> msg) -> Json.Decode.Decoder a -> Http.Expect msg
expect msg d =
    Http.expectStringResponse msg
        (\r ->
            case r of
                Http.BadStatus_ m body ->
                    error body

                Http.GoodStatus_ m body ->
                    decode d body

                _ ->
                    error "{msg : \"Něco se pokazilo\"}"
        )

decode : Json.Decode.Decoder a -> String -> Result String a
decode d b =
    Json.Decode.decodeString d b
        |> Result.mapError Json.Decode.errorToString


error : String -> Result String a
error b =
    Json.Decode.decodeString
        (Json.Decode.field "msg" Json.Decode.string)
        b
        |> Result.mapError Json.Decode.errorToString
        |> (\r ->
                case r of
                    Ok e ->
                        Err e

                    Err e ->
                        Err e
           )
