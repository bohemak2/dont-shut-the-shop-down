module Api.Call.Procedure exposing
    ( Kind(..)
    , Request
    , path
    , prepare
    )

import Api.Interface
import Entity
import Http
import Json.Decode
import Json.Encode


path =
    "rpc"


type Kind
    = Login
    | Decrement String Int
    | Increment String Int
    | Subscribe String Int
    | Unsubscribe String Int


toString : String -> Kind -> String
toString e k =
    case k of
        Login ->
            String.join "/" [ "_", "_" ++ Entity.delimiter ++ "login" ]

        Decrement a id ->
            String.join "/" [ String.fromInt id, a ++ Entity.delimiter ++ "decrement" ]

        Increment a id ->
            String.join "/" [ String.fromInt id, a ++ Entity.delimiter ++ "increment" ]

        Subscribe a id ->
            String.join "/" [ String.fromInt id, a ++ Entity.delimiter ++ "subscribe" ]

        Unsubscribe a id ->
            String.join "/" [ String.fromInt id, a ++ Entity.delimiter ++ "unsubscribe" ]


type alias Request a =
    { entity : String
    , decoder : Json.Decode.Decoder a
    , encode : Maybe Json.Encode.Value
    , kind : Kind
    }


prepare : Request a -> Api.Interface.Prepare a
prepare r =
    { decoder = r.decoder
    , method =
        if r.encode == Nothing then
            "GET"

        else
            "POST"
    , path = [ r.entity, toString r.entity r.kind ]
    , query = Nothing
    , body = r.encode |> Maybe.map Http.jsonBody |> Maybe.withDefault Http.emptyBody
    }
