module Api.Call.Rest exposing
    ( Kind(..)
    , Request
    , path
    , prepare
    )

import Api.Interface
import Http
import Json.Decode
import Json.Encode


path =
    "rest"


type Kind
    = Create
    | Delete Int
    | Update Int
    | Get Int
    | GetAll


type alias Request d =
    { entity : String
    , kind : Kind
    , decoder : Json.Decode.Decoder d
    , encode : Maybe Json.Encode.Value
    , filter : Maybe String
    , limit : Maybe Int
    , page : Maybe Int
    , search : Maybe String
    , sort : Maybe String
    }


prepare : Request a -> Api.Interface.Prepare a
prepare r =
    let
        ( ( method, p, query ), ( body, decoder ) ) =
            case r.kind of
                Create ->
                    case r.encode of
                        Just e ->
                            ( ( "POST", [], Nothing ), ( Http.jsonBody e, r.decoder ) )

                        Nothing ->
                            ( ( "POST", [], Nothing ), ( Http.emptyBody, r.decoder ) )

                Delete id ->
                    ( ( "DELETE", [ String.fromInt id ], Nothing ), ( Http.emptyBody, r.decoder ) )

                Update id ->
                    case r.encode of
                        Just e ->
                            ( ( "PATCH", [ String.fromInt id ], Nothing ), ( Http.jsonBody e, r.decoder ) )

                        Nothing ->
                            ( ( "PATCH", [ String.fromInt id ], Nothing ), ( Http.emptyBody, r.decoder ) )

                Get id ->
                    ( ( "GET", [ String.fromInt id ], r.filter ), ( Http.emptyBody, r.decoder ) )

                GetAll ->
                    ( ( "GET", [], r.filter ), ( Http.emptyBody, r.decoder ) )
    in
    { decoder = decoder
    , method = method
    , path = [ r.entity ] ++ p
    , query = query
    , body = body
    }
