module Api.Interface exposing
    ( Prepare
    , prefix
    , string
    , version
    )

import Http
import Json.Decode


prefix : String
prefix =
    "api"


version : String
version =
    "v0"


type alias Prepare a =
    { decoder : Json.Decode.Decoder a
    , method : String
    , path : List String
    , query : Maybe String
    , body : Http.Body
    }


string : { short : Int, long : Int }
string =
    { short = 1000, long = 10000 }
