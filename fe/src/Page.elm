module Page exposing
    ( Kind(..)
    , parse
    , view
    )

import Component.Session.Messages as Session
import Component.Session.Model as Session exposing (..)
import Component.Theme as Theme
import Component.Toast as Toast
import Dict
import Entity
import Html exposing (Html)
import Html.Attributes
import Html.Events
import Page.About
import Page.Contact
import Page.Home
import Page.Instructions
import Page.NotFound
import Page.Projects
import Page.Projects.Detail
import Page.User
import Url
import Url.Parser


type Kind
    = About
    | Contact
    | Home
    | Instructions
    | NotFound
    | Projects Page.Projects.Kind
    | User Page.User.Kind


parse : Url.Url -> Kind
parse url =
    Url.Parser.parse
        (Url.Parser.oneOf
            [ Url.Parser.map About Page.About.parser
            , Url.Parser.map Contact Page.Contact.parser
            , Url.Parser.map Home Page.Home.parser
            , Url.Parser.map Instructions Page.Instructions.parser
            , Url.Parser.map Projects Page.Projects.parser
            , Url.Parser.map User Page.User.parser
            ]
        )
        url
        |> Maybe.withDefault NotFound


view : Kind -> Session msg -> PageResponse msg
view page session =
    (case page of
        About ->
            Page.About.view

        Contact ->
            Page.Contact.view

        Home ->
            Page.Home.view

        Instructions ->
            Page.Instructions.view

        NotFound ->
            Page.NotFound.view

        Projects kind ->
            Page.Projects.view session kind

        User kind ->
            Page.User.view session kind
    )
        |> Tuple.mapSecond (viewLayout session)


viewLayout : Session msg -> Htmls msg -> Htmls msg
viewLayout s v =
    [ Html.node "style" [] [ Html.text Theme.render ]
    , Html.div (Theme.column ++ Theme.full)
        [ Html.nav (Theme.portion 0)
            [ Html.ul (Theme.row ++ Theme.centerX)
                [ Html.li Theme.spacing [ Html.a [ Html.Attributes.href Page.Home.build ] [ Html.text "Domů" ] ]
                , Html.li Theme.spacing
                    [ Html.a [ Html.Attributes.href (Page.Projects.build Page.Projects.All) ] [ Html.text "Projekty" ]
                    , Html.sub Theme.spacing [ Html.a [ Html.Attributes.href (Page.Projects.build <| Page.Projects.Detail Page.Projects.Detail.Create) ] [ Html.text "Nový" ] ]
                    ]
                , Html.li Theme.spacingX [ viewCredentials s ]
                ]
            ]
        , Html.section (Theme.column ++ Theme.centerY ++ Theme.centerSelf ++ Theme.spacing ++ Theme.portion 1) v
        , Html.nav (Theme.portion 0)
            [ Html.ul (Theme.row ++ Theme.centerX)
                [ Html.li Theme.spacing [ Html.a [ Html.Attributes.href Page.Instructions.build ] [ Html.text "Návod" ] ]
                , Html.li Theme.spacing [ Html.a [ Html.Attributes.href Page.Contact.build ] [ Html.text "Kontakt" ] ]
                , Html.li Theme.spacing [ Html.a [ Html.Attributes.href Page.About.build ] [ Html.text "O nás" ] ]
                ]
            ]
        ]
    ]
        ++ viewToasts s


viewCredentials : Session msg -> Html msg
viewCredentials s =
    Html.ul (Theme.row ++ Theme.spacingX) <|
        List.map (Html.li Theme.spacing)
            (case ( s.credentials, s.credentials |> Maybe.andThen .id ) of
                ( Just c, Just _ ) ->
                    [ [ Html.a [ Html.Attributes.href (Page.User.build Page.User.Details) ] [ Page.User.viewCredentials s c False ] ]
                    , [ Html.a [ Html.Attributes.href "#", Html.Events.onClick (s.msg <| Session.Request Entity.Owner Session.Logout) ] [ Html.text "Odhlásit" ]
                      ]
                    ]

                _ ->
                    [ [ Html.a [ Html.Attributes.href (Page.User.build Page.User.Details) ] [ Html.text "Přihlásit" ] ]
                    , [ Html.a [ Html.Attributes.href (Page.User.build Page.User.Registration) ] [ Html.text "Registrovat" ] ]
                    ]
            )


viewToasts : Session msg -> Htmls msg
viewToasts s =
    if Dict.isEmpty s.toasts then
        []

    else
        [ Html.ul Theme.toasts <|
            List.map
                (\t -> Html.li (Theme.toast ++ Theme.centerText) [ Toast.view t ])
                (Dict.values s.toasts)
        ]
