module Entity exposing
    ( Field
    , Fields
    , Kind(..)
    , Rule
    , Rules
    , delimiter
    )


type Kind
    = Message
    | Owner
    | Project


delimiter =
    "$"


type alias Rule =
    ( String, String )


type alias Rules =
    List Rule


type alias Field =
    ( String, String )


type alias Fields =
    List Field



-- TODO: We should keep everything typed... So, also DB Rules should be generated from, which can be found useful in forms for frontend-evaluation.
-- type Attribute = String StringModifier | Int IntModifier | BoolModifier | Ignore
-- type StringModifier = Email | Vat | Hash..., type IntModifier = Posix | Fk ...
-- create: Attribute -> String
