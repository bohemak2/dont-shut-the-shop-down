module Entity.Message exposing
    ( Model
    , db
    , decode
    , decodeAll
    , encode
    , form
    , fromForm
    , path
    )

import Dict
import Entity exposing (Fields, Rules)
import Json.Decode
import Json.Decode.Extra
import Json.Encode
import Time


path : String
path =
    "message"


type alias Model =
    { id : Int
    , projectId : Int
    , created : Time.Posix
    , text : String
    }


db : Rules
db =
    [ ( "projectId", "int$fk" )
    , ( "created", "int$posixOnCreate" )
    , ( "text", "string" )
    ]


form : Fields
form =
    [ ( "text", "Zpráva" )
    ]


fromForm : Int -> Fields -> Model
fromForm pId f =
    let
        results =
            Dict.fromList f
    in
    { id = 0
    , projectId = pId
    , created = Time.millisToPosix 0
    , text = Dict.get "text" results |> Maybe.withDefault ""
    }


decodeAll : Json.Decode.Decoder (List Model)
decodeAll =
    Json.Decode.list decode


decode : Json.Decode.Decoder Model
decode =
    Json.Decode.succeed Model
        |> Json.Decode.Extra.andMap (Json.Decode.field "id" Json.Decode.int)
        |> Json.Decode.Extra.andMap (Json.Decode.field "projectId" Json.Decode.int)
        |> Json.Decode.Extra.andMap (Json.Decode.field "created" (Json.Decode.int |> Json.Decode.map Time.millisToPosix))
        |> Json.Decode.Extra.andMap (Json.Decode.field "text" Json.Decode.string)



-- TODO: Showing we don't need functors
--Json.Decode.map2 Model (Json.Decode.field "id" Json.Decode.int) (Json.Decode.field "projectId" Json.Decode.int)
--    |> Json.Decode.map2 (|>) (Json.Decode.field "created" Json.Decode.int)
--    |> Json.Decode.map2 (|>) (Json.Decode.field "text" Json.Decode.string)


encode : Model -> Json.Encode.Value
encode m =
    Json.Encode.object
        [ ( "projectId", Json.Encode.int m.projectId )
        , ( "created", Json.Encode.int <| Time.posixToMillis m.created )
        , ( "text", Json.Encode.string m.text )
        ]
