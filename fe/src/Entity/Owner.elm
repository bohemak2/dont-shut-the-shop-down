module Entity.Owner exposing
    ( Model
    , OwnerId
    , db
    , decode
    , decodeAll
    , encode
    , form
    , formLogin
    , fromForm
    , path
    )

import Dict
import Entity exposing (Fields, Rules)
import Json.Decode
import Json.Decode.Extra
import Json.Encode
import Time


path : String
path =
    "owner"


type alias OwnerId =
    Int


type alias Model =
    { id : OwnerId
    , vat : String
    , name : String
    , email : String
    , password : String
    , created : Time.Posix
    }


db : Rules
db =
    [ ( "vat", "string$vatUnique" )
    , ( "name", "string" )
    , ( "email", "string$emailUnique" )
    , ( "password", "string$hash" )
    , ( "created", "int$posixOnCreate" )
    ]
        ++ [ ( "procedure", "_$login" ) ]


form : Fields
form =
    [ ( "vat", "IČO" )
    , ( "name", "Jméno" )
    , ( "email", "Email" )
    , ( "password", "Heslo" )
    ]


formLogin : Fields
formLogin =
    [ ( "email", "Email" )
    , ( "password", "Heslo" )
    ]



-- TODO: Maybe Model, error in frontend before send


fromForm : Fields -> Model
fromForm f =
    let
        results =
            Dict.fromList f
    in
    { id = 0
    , vat = Dict.get "vat" results |> Maybe.withDefault ""
    , name = Dict.get "name" results |> Maybe.withDefault ""
    , email = Dict.get "email" results |> Maybe.withDefault ""
    , password = Dict.get "password" results |> Maybe.withDefault ""
    , created = Time.millisToPosix 0
    }


decodeAll : Json.Decode.Decoder (List Model)
decodeAll =
    Json.Decode.list decode


decode : Json.Decode.Decoder Model
decode =
    Json.Decode.succeed Model
        |> Json.Decode.Extra.andMap (Json.Decode.field "id" Json.Decode.int)
        |> Json.Decode.Extra.andMap (Json.Decode.field "vat" Json.Decode.string)
        |> Json.Decode.Extra.andMap (Json.Decode.field "name" Json.Decode.string)
        |> Json.Decode.Extra.andMap (Json.Decode.field "email" Json.Decode.string)
        |> Json.Decode.Extra.andMap (Json.Decode.field "password" Json.Decode.string)
        |> Json.Decode.Extra.andMap (Json.Decode.field "created" (Json.Decode.int |> Json.Decode.map Time.millisToPosix))


encode : Model -> Json.Encode.Value
encode m =
    Json.Encode.object
        [ ( "vat", Json.Encode.string m.vat )
        , ( "name", Json.Encode.string m.name )
        , ( "email", Json.Encode.string m.email )
        , ( "password", Json.Encode.string m.password )
        ]
