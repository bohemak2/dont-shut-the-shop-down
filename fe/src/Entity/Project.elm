module Entity.Project exposing
    ( Model
    , Price
    , ProjectId
    , db
    , decode
    , decodeAll
    , encode
    , form
    , fromForm
    , path
    , toForm
    )

import Dict
import Entity exposing (Fields, Rules)
import Json.Decode
import Json.Decode.Extra
import Json.Encode
import Time
import Utils.Time.Extra


path : String
path =
    "project"


type alias Price =
    Int


type alias ProjectId =
    Int


type alias Model =
    { id : ProjectId
    , ownerId : Int
    , name : String

    --, image : String TODO: elm/file
    , summary : String
    , created : Time.Posix
    , until : Time.Posix
    , price1 : Price
    , price2 : Price
    , bankAccount : String
    , bankCode : String
    , bankTrust : Maybe Bool
    , bankTrustUrl : String
    , views : Maybe Int
    , interests : Maybe Int
    }


db : Rules
db =
    [ ( "ownerId", "int$fk" )
    , ( "name", "string" )

    --, ( "image", "string$base64" )
    , ( "summary", "string$long" )
    , ( "created", "int$posixOnCreate" )
    , ( "until", "int$posix" )
    , ( "price1", "int" )
    , ( "price2", "int" )
    , ( "bankAccount", "string$bankAccount" )
    , ( "bankCode", "string$bankCode" )
    , ( "bankTrust", "ignore" )
    , ( "bankTrustUrl", "string$url" )
    , ( "views", "ignore" )
    , ( "interests", "ignore" )
    ]
        ++ [ ( "procedure", "views$increment" )
           , ( "procedure", "interests$increment" )
           , ( "procedure", "interests$decrement" )
           ]


form : Fields
form =
    [ ( "name", "Název" )

    --, ( "image", "Náhled" )
    , ( "summary", "Popis" )
    , ( "until", "do" )
    , ( "price1", "Nižší příspěvek" )
    , ( "price2", "Vyšší příspěvek" )
    , ( "bankAccount", "Číslo účtu" )
    , ( "bankCode", "Číslo banky" )
    , ( "bankTrustUrl", "Odkaz na pravdivost údajů" )
    ]


toForm : Time.Zone -> Model -> Fields
toForm z m =
    [ ( "name", m.name )

    --, ( "image", m.image )
    , ( "summary", m.summary )
    , ( "until", Utils.Time.Extra.toInput z m.until )
    , ( "price1", String.fromInt m.price1 )
    , ( "price2", String.fromInt m.price2 )
    , ( "bankAccount", m.bankAccount )
    , ( "bankCode", m.bankCode )
    , ( "bankTrustUrl", m.bankTrustUrl )
    ]



-- TODO: Maybe Model, error in frontend before send


fromForm : Int -> Fields -> Model
fromForm ownerId f =
    let
        results =
            Dict.fromList f
    in
    { id = 0
    , ownerId = ownerId
    , name = Dict.get "name" results |> Maybe.withDefault ""

    --, image = Dict.get "image" results |> Maybe.withDefault ""
    , summary = Dict.get "summary" results |> Maybe.withDefault ""
    , created = Time.millisToPosix 0
    , until = Dict.get "until" results |> Maybe.andThen Utils.Time.Extra.fromInput |> Maybe.withDefault (Time.millisToPosix 0)
    , price1 = Dict.get "price1" results |> Maybe.andThen String.toInt |> Maybe.withDefault 0
    , price2 = Dict.get "price2" results |> Maybe.andThen String.toInt |> Maybe.withDefault 0
    , bankAccount = Dict.get "bankAccount" results |> Maybe.withDefault ""
    , bankCode = Dict.get "bankCode" results |> Maybe.withDefault ""
    , bankTrust = Nothing
    , bankTrustUrl = Dict.get "bankTrustUrl" results |> Maybe.withDefault ""
    , views = Nothing
    , interests = Nothing
    }


decodeAll : Json.Decode.Decoder (List Model)
decodeAll =
    Json.Decode.list decode


decode : Json.Decode.Decoder Model
decode =
    Json.Decode.succeed Model
        |> Json.Decode.Extra.andMap (Json.Decode.field "id" Json.Decode.int)
        |> Json.Decode.Extra.andMap (Json.Decode.field "ownerId" Json.Decode.int)
        |> Json.Decode.Extra.andMap (Json.Decode.field "name" Json.Decode.string)
        --|> Json.Decode.Extra.andMap (Json.Decode.field "image" Json.Decode.string)
        |> Json.Decode.Extra.andMap (Json.Decode.field "summary" Json.Decode.string)
        |> Json.Decode.Extra.andMap (Json.Decode.field "created" (Json.Decode.int |> Json.Decode.map Time.millisToPosix))
        |> Json.Decode.Extra.andMap (Json.Decode.field "until" (Json.Decode.int |> Json.Decode.map Time.millisToPosix))
        |> Json.Decode.Extra.andMap (Json.Decode.field "price1" Json.Decode.int)
        |> Json.Decode.Extra.andMap (Json.Decode.field "price2" Json.Decode.int)
        |> Json.Decode.Extra.andMap (Json.Decode.field "bankAccount" Json.Decode.string)
        |> Json.Decode.Extra.andMap (Json.Decode.field "bankCode" Json.Decode.string)
        |> Json.Decode.Extra.andMap (Json.Decode.maybe <| Json.Decode.field "bankTrust" Json.Decode.bool)
        |> Json.Decode.Extra.andMap (Json.Decode.field "bankTrustUrl" Json.Decode.string)
        |> Json.Decode.Extra.andMap (Json.Decode.maybe <| Json.Decode.field "views" Json.Decode.int)
        |> Json.Decode.Extra.andMap (Json.Decode.maybe <| Json.Decode.field "interests" Json.Decode.int)


encode : Model -> Json.Encode.Value
encode m =
    Json.Encode.object
        [ ( "ownerId", Json.Encode.int m.ownerId )
        , ( "name", Json.Encode.string m.name )

        --, ( "image", Json.Encode.string m.image )
        , ( "summary", Json.Encode.string m.summary )
        , ( "until", Json.Encode.int <| Time.posixToMillis m.until )
        , ( "price1", Json.Encode.int m.price1 )
        , ( "price2", Json.Encode.int m.price2 )
        , ( "bankAccount", Json.Encode.string m.bankAccount )
        , ( "bankCode", Json.Encode.string m.bankCode )
        , ( "bankTrustUrl", Json.Encode.string m.bankTrustUrl )
        ]
