port module Entity.Notification exposing
    ( Model
    , db
    , decode
    , encode
    , path
    , putInterest
    , receiveInterests
    )

import Entity exposing (Rules)
import Entity.Project
import Json.Decode
import Json.Decode.Extra
import Json.Encode


path : String
path =
    "notification"


type alias Keys =
    { p256dh : String
    , auth : String
    }


type alias Model =
    { projectId : Entity.Project.ProjectId
    , endpoint : String
    , keys : Keys
    }


db : Rules
db =
    [ ( "projectId", "ignore" )
    ]
        ++ [ ( "procedure", Entity.Project.path ++ "$subscribe" )
           , ( "procedure", Entity.Project.path ++ "$unsubscribe" )
           ]


encode : Model -> Json.Encode.Value
encode m =
    Json.Encode.object
        [ ( "projectId", Json.Encode.int m.projectId )
        , ( "endpoint", Json.Encode.string m.endpoint )
        , ( "keys"
          , Json.Encode.object
                [ ( "p256dh", Json.Encode.string m.keys.p256dh )
                , ( "auth", Json.Encode.string m.keys.auth )
                ]
          )
        ]


decode : Json.Decode.Decoder Model
decode =
    Json.Decode.succeed Model
        |> Json.Decode.Extra.andMap (Json.Decode.field "projectId" Json.Decode.int)
        |> Json.Decode.Extra.andMap (Json.Decode.field "endpoint" Json.Decode.string)
        |> Json.Decode.Extra.andMap
            (Json.Decode.field "keys"
                (Json.Decode.succeed Keys
                    |> Json.Decode.Extra.andMap (Json.Decode.field "p256dh" Json.Decode.string)
                    |> Json.Decode.Extra.andMap (Json.Decode.field "auth" Json.Decode.string)
                )
            )


port putInterest : Int -> Cmd msg



-- (Append or Remove, ProjectId, Model Stringify)


port receiveInterests : (( Bool, Int, String ) -> msg) -> Sub msg
