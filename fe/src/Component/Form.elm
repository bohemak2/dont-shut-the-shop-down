module Component.Form exposing
    ( Model
    , init
    , reset
    , update
    , updateAll
    , values
    , view
    )

import Api.Interface
import Component.Theme as Theme
import Dict exposing (Dict)
import Entity exposing (Fields, Rule, Rules)
import Html exposing (Html)
import Html.Attributes
import Html.Events


type Value
    = Number
    | Text
    | Bool
    | Date
    | Email
    | Url
    | Hash
    | Ignore


type alias Type =
    String


type alias Modifier =
    String


fromString : Type -> Maybe Modifier -> Kind
fromString s m =
    case s of
        "string" ->
            case m of
                Just "long" ->
                    TextArea

                Just "email" ->
                    Input Email

                Just "emailUnique" ->
                    Input Email

                Just "hash" ->
                    Input Hash

                Just "url" ->
                    Input Url

                _ ->
                    Input Text

        "int" ->
            case m of
                Just "posix" ->
                    Input Date

                _ ->
                    Input Number

        "bool" ->
            Input Bool

        _ ->
            Input Ignore


type Kind
    = Input Value
    | TextArea


type alias FieldId =
    String


type alias Model =
    { inputs : Dict FieldId (Input InputModel)
    , order : List FieldId
    }


type alias Input a =
    { a | kind : Kind, label : Maybe String, required : Bool }


type alias InputModel =
    { kind : Kind
    , label : Maybe String
    , required : Bool
    , value : Maybe String
    , valueOld : Maybe String
    }


empty : Kind -> Maybe String -> InputModel
empty k l =
    { kind = k
    , label = l
    , required = False
    , value = Nothing
    , valueOld = Nothing
    }


init : Fields -> Rules -> Model
init f r =
    let
        form =
            Dict.fromList f

        rules =
            Dict.fromList r

        inputs =
            let
                go k v =
                    rule ( k, v )
                        |> (\( k2, v2 ) ->
                                empty v2 (Dict.get k2 form)
                           )
            in
            Dict.intersect rules form
                |> Dict.map go
    in
    { inputs = inputs
    , order = f |> List.map Tuple.first
    }


rule : Rule -> ( String, Kind )
rule ( id, r ) =
    case String.split Entity.delimiter r of
        t :: m :: _ ->
            ( id, fromString t (Just m) )

        t :: _ ->
            ( id, fromString t Nothing )

        _ ->
            ( id, Input Ignore )



-- UPDATE


update : FieldId -> String -> Model -> Model
update id v m =
    case Dict.get id m.inputs of
        Just i ->
            let
                newInput =
                    { i | value = Just v }
            in
            { m | inputs = Dict.insert id newInput m.inputs }

        _ ->
            m


updateAll : Fields -> Model -> Model
updateAll f m =
    Dict.fromList f
        |> Dict.foldl update m


reset : Model -> Model
reset m =
    let
        newInputs =
            Dict.map (\k v -> { v | value = Nothing }) m.inputs
    in
    { m | inputs = newInputs }


values : Model -> Fields
values model =
    let
        go k v =
            ( k, v.value |> Maybe.withDefault "" )
    in
    Dict.map go model.inputs
        |> Dict.values



-- VIEW


view : (FieldId -> String -> msg) -> msg -> Model -> Html msg
view msgChange msgSubmit model =
    viewInputs msgChange msgSubmit model.order model.inputs


viewInputs : (FieldId -> String -> msg) -> msg -> List FieldId -> Dict FieldId (Input InputModel) -> Html msg
viewInputs msgChange msgSubmit order inputs =
    order
        |> List.reverse
        |> List.map
            (\id ->
                case Dict.get id inputs of
                    Just t ->
                        case t.kind of
                            Input k ->
                                [ viewInput msgChange id k t ]

                            TextArea ->
                                [ viewTextArea msgChange id t ]

                    Nothing ->
                        []
            )
        |> List.concat
        |> List.append [ viewSubmit ]
        |> List.reverse
        |> Html.form [ Html.Events.onSubmit msgSubmit ]


viewInput : (FieldId -> String -> msg) -> FieldId -> Value -> InputModel -> Html msg
viewInput msgChange id t m =
    let
        attrs =
            basicAttrs msgChange id m
                ++ (case t of
                        Number ->
                            [ Html.Attributes.type_ "number" ]

                        Text ->
                            [ Html.Attributes.type_ "text" ]

                        Bool ->
                            [ Html.Attributes.type_ "checkbox" ]

                        Date ->
                            [ Html.Attributes.type_ "date" ]

                        Email ->
                            [ Html.Attributes.type_ "email" ]

                        Hash ->
                            [ Html.Attributes.type_ "password" ]

                        Url ->
                            [ Html.Attributes.type_ "url" ]

                        _ ->
                            []
                   )
    in
    Html.div
        []
        (label m ++ [ Html.input attrs [] ])


viewTextArea : (FieldId -> String -> msg) -> FieldId -> InputModel -> Html msg
viewTextArea msgChange id m =
    let
        attrs =
            basicAttrs msgChange id m
    in
    Html.div
        []
        [ Html.label []
            (label m ++ counter m)
        , Html.textarea attrs []
        ]



--


viewSubmit : Html msg
viewSubmit =
    Html.input
        ([ Html.Attributes.type_ "submit"
         ]
            ++ Theme.button
        )
        [ Html.text "Odeslat" ]



--


label : InputModel -> List (Html msg)
label m =
    case m.label of
        Just l ->
            [ Html.label [] [ Html.text l ] ]

        Nothing ->
            []


counter : InputModel -> List (Html msg)
counter m =
    let
        c =
            m.value
                |> Maybe.map String.length
                |> Maybe.withDefault 0

        max =
            case m.kind of
                Input Text ->
                    Just <| Api.Interface.string.short

                TextArea ->
                    Just <| Api.Interface.string.long

                _ ->
                    Nothing
    in
    case max of
        Just max_ ->
            [ Html.small []
                [ Html.text <| String.fromInt c
                , Html.text "/"
                , Html.text <| String.fromInt max_
                ]
            ]

        Nothing ->
            []


basicAttrs : (FieldId -> String -> msg) -> FieldId -> InputModel -> List (Html.Attribute msg)
basicAttrs msgChange id m =
    let
        attrValue =
            case m.value of
                Just v ->
                    [ Html.Attributes.value v ]

                Nothing ->
                    []
    in
    attrValue
        ++ [ Html.Events.onInput (msgChange id)
           , Html.Attributes.id id
           , Html.Attributes.required m.required
           , Html.Attributes.autocomplete False
           ]
