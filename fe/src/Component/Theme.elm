module Component.Theme exposing (Attributes, button, centerSelf, centerText, centerX, centerY, column, down, downSelf, flex, fonts, full, portion, render, row, space, spacing, spacingX, spacingY, style, toast, toasts, up, upSelf)

import Html exposing (Attribute)
import Html.Attributes


type alias Attributes msg =
    List (Attribute msg)


style : String -> String -> Attribute msg
style =
    Html.Attributes.style



-- STYLE


render : String
render =
    ("html,body{height: 100%;margin: 0 auto;line-height:1.33;background:" ++ background ++ ";color:" ++ color ++ ";font-family:" ++ font ++ "}")
        ++ "body{max-width:768px}*{box-sizing:border-box}"
        ++ ("a{color:" ++ colorLink ++ ";}a:hover{text-decoration:underline;}nav a{text-decoration:none;}")
        ++ ("input,textarea{background:transparent;border-radius:" ++ radius ++ ";padding:" ++ space ++ ";margin:" ++ space ++ ";border:0;box-shadow:" ++ shadow ++ ";}input:focus,textarea:focus{outline:none}input[type=submit],button{font-size:.95em;line-height:1}textarea{min-width:280px}")
        ++ "label+small{display:block;font-size:.5em;}"
        ++ "label{min-width:4em;display:inline-block}"
        ++ ("hr{border:1px solid " ++ colorDisabled ++ ";box-shadow:none}")



-- VARIABLES


background : String
background =
    "#efefef"


color : String
color =
    "#131313"


colorLink : String
colorLink =
    "#0c0c0c"


colorDisabled : String
colorDisabled =
    "#f1f1f1f1"


font : String
font =
    "system-ui,-apple-system,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji'"


radius : String
radius =
    ".8em"


shadow : String
shadow =
    "0 0 .1em .05em " ++ color


space : String
space =
    ".55em"



-- BASICS


flex : Attribute msg
flex =
    style "display" "flex"


fonts : Attributes msg
fonts =
    [ Html.Attributes.style "font-family" font ]



--  ALIGNMENTS


centerX : Attributes msg
centerX =
    [ style "justify-content" "center"
    ]


centerY : Attributes msg
centerY =
    [ style "align-items" "center"
    ]


centerSelf : Attributes msg
centerSelf =
    [ style "align-self" "center"
    ]


centerText : Attributes msg
centerText =
    [ style "text-align" "center"
    ]


up : Attributes msg
up =
    [ style "align-items" "flex-start"
    ]


upSelf : Attributes msg
upSelf =
    [ style "align-self" "flex-start"
    ]


down : Attributes msg
down =
    [ style "align-items" "flex-end"
    ]


downSelf : Attributes msg
downSelf =
    [ style "align-self" "flex-end"
    ]



-- ITEMS


row : Attributes msg
row =
    [ flex
    , style "flex-direction" "row"
    , style "flex-wrap" "wrap"
    , style "list-style-type" "none"
    , style "padding" "0"
    , style "width" "100%"
    ]


column : Attributes msg
column =
    [ flex
    , style "flex-direction" "column"
    , style "list-style-type" "none"
    , style "padding" "0"
    ]


full : Attributes msg
full =
    [ style "align-items" "stretch"
    , style "height" "100%"
    , style "width" "100%"
    ]


portion : Int -> Attributes msg
portion i =
    [ style "flex-grow" <| String.fromInt i
    ]



-- SPACING


spacing : Attributes msg
spacing =
    [ style "padding" space ]


spacingX : Attributes msg
spacingX =
    [ style "padding-left" space
    , style "padding-right" space
    ]


spacingY : Attributes msg
spacingY =
    [ style "padding-top" space
    , style "padding-bottom" space
    ]



-- COMPONENTS


button : Attributes msg
button =
    [ style "padding" ".33em .5em"
    , style "margin" ".25em .33em"
    , style "text-decoration" "none"
    , style "background" "transparent"
    , style "border-color" "transparent"
    , style "border-radius" ".75em"
    , style "box-shadow" shadow
    , style "color" color
    , style "cursor" "pointer"
    ]


link : Attributes msg
link =
    [ style "color" colorLink
    , style "cursor" "pointer"
    ]


toasts : Attributes msg
toasts =
    column
        ++ centerY
        ++ [ style "position" "fixed"
           , style "top" "0"
           , style "left" "0"
           , style "width" "100%"
           ]


toast : Attributes msg
toast =
    button
        ++ [ style "background" "white"
           , style "max-width" "280px"
           , style "cursor" "normal"
           , style "box-shadow" (".025em .05em .2em .025em " ++ color)
           ]
