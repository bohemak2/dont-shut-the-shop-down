module Component.Session.Messages exposing
    ( Msg(..)
    , Transformation(..)
    , Type(..)
    , transform
    , transformationFromId
    , transformationToId
    )

import Entity
import Entity.Owner


type alias Value =
    String


type Type
    = Create (Maybe Entity.Owner.OwnerId)
    | Read Int
    | ReadAll Int
    | Update Int
    | Delete Int
    | Credentials
    | Login (Maybe String)
    | Logout
    | View Int
    | Interest Int
    | Subscription ( Bool, Int, Value )


type Msg
    = Form Entity.Kind Type String String
    | Request Entity.Kind Type


type Transformation
    = Base64Credentials


transform : ( Int, Value ) -> Maybe Msg
transform ( id, v ) =
    case transformationFromId id of
        Just Base64Credentials ->
            Just <| Request Entity.Owner (Login (Just v))

        Nothing ->
            Nothing


transformationToId : Transformation -> Int
transformationToId b =
    case b of
        Base64Credentials ->
            1


transformationFromId : Int -> Maybe Transformation
transformationFromId i =
    case i of
        1 ->
            Just <| Base64Credentials

        _ ->
            Nothing
