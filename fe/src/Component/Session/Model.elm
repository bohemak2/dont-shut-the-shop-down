module Component.Session.Model exposing
    ( Extended
    , Flags
    , Htmls
    , PageResponse
    , Session
    )

import Api
import Browser.Navigation
import Component.Credentials exposing (Credentials)
import Component.Form as Form
import Component.Session.Messages as Session
import Component.Toast as Toast
import Dict exposing (Dict)
import Entity.Message
import Entity.Owner
import Entity.Project
import Html exposing (Html)
import Time
import Url


type alias PageResponse msg =
    ( String, Htmls msg )


type alias Htmls msg =
    List (Html msg)


type alias Extended =
    Bool


type alias Flags =
    List Entity.Project.ProjectId


type alias Session msg =
    { credentials : Maybe Credentials
    , flags : Flags
    , key : Browser.Navigation.Key
    , loaded : ( Bool, Bool )
    , login : Form.Model
    , message : Form.Model
    , messages : Dict Int (List Entity.Message.Model)
    , msg : Session.Msg -> msg
    , cmd : Cmd msg -> msg
    , owner : Form.Model
    , owners : Dict Int Entity.Owner.Model
    , ownersRequest : Api.Request (List Entity.Owner.Model) msg
    , project : Form.Model
    , projects : Dict Int Entity.Project.Model
    , projectsRequest : Api.Request (List Entity.Project.Model) msg
    , toasts : Dict String Toast.Model
    , url : Url.Url
    , zone : Time.Zone
    }
