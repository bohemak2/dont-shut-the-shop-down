module Session exposing
    ( init
    , load
    , update
    )

import Api
import Api.Call.Procedure
import Api.Call.Rest
import Browser.Navigation
import Component.Credentials exposing (Credentials)
import Component.Form as Form
import Component.Model exposing (Model)
import Component.Session.Messages as Session
import Component.Session.Model exposing (Flags, Session)
import Component.Toast as Toast
import Dict exposing (Dict)
import Entity
import Entity.Message
import Entity.Notification
import Entity.Owner
import Entity.Project
import Json.Decode
import Maybe exposing (Maybe(..))
import Page
import Page.Projects
import Page.Projects.Detail
import Page.User
import Time
import Url


init : (Session.Msg -> msg) -> (Cmd msg -> msg) -> Flags -> Browser.Navigation.Key -> Url.Url -> ((Model msg -> ( Model msg, Cmd msg )) -> msg) -> Session msg
init msgS msgC flags key url msgU =
    { credentials = Nothing
    , flags = flags
    , key = key
    , loaded = ( False, False )
    , login = Form.init Entity.Owner.formLogin Entity.Owner.db
    , message = Form.init Entity.Message.form Entity.Message.db
    , messages = Dict.empty
    , msg = msgS
    , cmd = msgC
    , owner = Form.init Entity.Owner.form Entity.Owner.db
    , owners = Dict.empty
    , ownersRequest =
        { kind =
            Api.Rest
                { entity = Entity.Owner.path
                , kind = Api.Call.Rest.GetAll
                , decoder = Entity.Owner.decodeAll
                , encode = Nothing
                , filter = Nothing
                , limit = Nothing
                , page = Nothing
                , search = Nothing
                , sort = Nothing
                }
        , change = updateEntityAll msgU (\( p, s ) os -> ( ( p, { s | owners = os, loaded = Tuple.mapFirst (always True) s.loaded } ), Cmd.none ) |> load msgS msgU)
        , timeout = Nothing
        , tracker = Nothing
        }
    , project = Form.init Entity.Project.form Entity.Project.db
    , projects = Dict.empty
    , projectsRequest =
        { kind =
            Api.Rest
                { entity = Entity.Project.path
                , kind = Api.Call.Rest.GetAll
                , decoder = Entity.Project.decodeAll
                , encode = Nothing
                , filter = Nothing
                , limit = Nothing
                , page = Nothing -- TODO: Paging, then Dict.union on updateEntityAll, beware of DELETE (now Api.call after delete)
                , search = Nothing
                , sort = Nothing
                }
        , change = updateEntityAll msgU (\( p, s ) ps -> ( ( p, { s | projects = ps, loaded = Tuple.mapSecond (always True) s.loaded } ), Cmd.none ) |> load msgS msgU)
        , timeout = Nothing
        , tracker = Nothing
        }
    , toasts = Dict.empty
    , url = url
    , zone = Time.customZone 0 []
    }


update : (Session.Msg -> msg) -> Session.Msg -> Session msg -> ((Model msg -> ( Model msg, Cmd msg )) -> msg) -> ( Session msg, Cmd msg )
update msgS msg s msgU =
    case msg of
        Session.Form k t id v ->
            case k of
                Entity.Project ->
                    ( { s | project = Form.update id v s.project }, Cmd.none )

                Entity.Owner ->
                    case t of
                        Session.Login Nothing ->
                            ( { s | login = Form.update id v s.login }, Cmd.none )

                        _ ->
                            ( { s | owner = Form.update id v s.owner }, Cmd.none )

                Entity.Message ->
                    ( { s | message = Form.update id v s.message }, Cmd.none )

        Session.Request k t ->
            let
                s_ =
                    case t of
                        Session.Login (Just h) ->
                            { s | credentials = Just { hash = h, id = Nothing } }

                        Session.Logout ->
                            { s | login = Form.reset s.login, credentials = Nothing }

                        Session.Subscription ( append, projectId, _ ) ->
                            { s
                                | flags =
                                    if append then
                                        List.append [ projectId ] s.flags

                                    else
                                        List.filter ((/=) projectId) s.flags
                            }

                        _ ->
                            s

                rest { entity, kind, decoder, encode, filter, f, up } =
                    Api.call s_
                        { kind =
                            Api.Rest
                                { entity = entity
                                , kind = kind
                                , decoder = decoder
                                , encode = encode
                                , filter = filter
                                , limit = Nothing
                                , page = Nothing
                                , search = Nothing
                                , sort = Nothing
                                }
                        , change = up msgU f
                        , timeout = Nothing
                        , tracker = Nothing
                        }

                rpc { entity, decoder, encode, kind, f, up } =
                    Api.call s_
                        { kind =
                            Api.Procedure
                                { entity = entity
                                , decoder = decoder
                                , encode = encode
                                , kind = kind
                                }
                        , change = up msgU f
                        , timeout = Nothing
                        , tracker = Nothing
                        }

                cmd =
                    case k of
                        Entity.Project ->
                            case t of
                                Session.Create (Just ownerId) ->
                                    rest
                                        { entity = Entity.Project.path
                                        , kind = Api.Call.Rest.Create
                                        , decoder = Entity.Project.decode
                                        , encode = Just <| Entity.Project.encode <| Entity.Project.fromForm ownerId <| Form.values s.project
                                        , filter = Nothing
                                        , f =
                                            \( p, s__ ) np ->
                                                ( ( p, { s__ | projects = Dict.insert np.id np s__.projects, project = Form.reset s__.project } )
                                                , Cmd.batch
                                                    [ Toast.success "project-create" <| "Projekt byl úspěšně vytvořen"
                                                    , Browser.Navigation.pushUrl s__.key (Page.Projects.build <| Page.Projects.Detail <| Page.Projects.Detail.Read np.id)
                                                    ]
                                                )
                                        , up = updateEntity
                                        }

                                Session.Update id_ ->
                                    case s_.credentials |> Maybe.andThen .id of
                                        Just userId ->
                                            rest
                                                { entity = Entity.Project.path
                                                , kind = Api.Call.Rest.Update id_
                                                , decoder = Entity.Project.decode
                                                , encode = Just <| Entity.Project.encode <| Entity.Project.fromForm userId <| Form.values s.project
                                                , filter = Nothing
                                                , f =
                                                    \( p, s__ ) np ->
                                                        ( ( p, { s__ | projects = Dict.insert np.id np s__.projects } )
                                                        , Cmd.batch
                                                            [ Toast.success "project-save" <| "Projekt byl úspěšně uložen"
                                                            , Browser.Navigation.pushUrl s__.key (Page.Projects.build <| Page.Projects.Detail <| Page.Projects.Detail.Read np.id)
                                                            ]
                                                        )
                                                , up = updateEntity
                                                }

                                        Nothing ->
                                            Cmd.none

                                Session.Delete id_ ->
                                    rest
                                        { entity = Entity.Project.path
                                        , kind = Api.Call.Rest.Delete id_
                                        , decoder = Json.Decode.value
                                        , encode = Nothing
                                        , filter = Nothing
                                        , f =
                                            \( p, s__ ) _ ->
                                                ( ( p, { s__ | projects = Dict.remove id_ s__.projects } )
                                                , Cmd.batch
                                                    [ Toast.info "project-delete" <| "Projekt byl úspěšně smazán"
                                                    , Api.call s__ s__.projectsRequest
                                                    ]
                                                )
                                        , up = updateEntity
                                        }

                                Session.View id ->
                                    rpc
                                        { entity = Entity.Project.path
                                        , decoder = Entity.Project.decode
                                        , encode = Nothing
                                        , kind = Api.Call.Procedure.Increment "views" id
                                        , f = \( p, s__ ) np -> ( ( p, { s__ | projects = Dict.insert np.id np s__.projects } ), Cmd.none )
                                        , up = updateEntity
                                        }

                                Session.Interest id ->
                                    Entity.Notification.putInterest id

                                Session.Subscription ( append, projectId, keys ) ->
                                    let
                                        ( t_, k_, m_ ) =
                                            case append of
                                                True ->
                                                    ( Api.Call.Procedure.Subscribe, Api.Call.Procedure.Increment, "Přihlášení k odběru zpráv proběhlo úspěšně, budete notifikován systémovou zprávou." )

                                                False ->
                                                    ( Api.Call.Procedure.Unsubscribe, Api.Call.Procedure.Decrement, "Odhlášení z odběru zpráv proběhlo úspěšně" )
                                    in
                                    Cmd.batch
                                        [ rpc
                                            { entity = Entity.Notification.path
                                            , decoder = Json.Decode.value
                                            , encode = Maybe.map Entity.Notification.encode <| Result.toMaybe <| Json.Decode.decodeString Entity.Notification.decode keys
                                            , kind = t_ Entity.Project.path projectId
                                            , f =
                                                \( p, s__ ) _ ->
                                                    ( ( p, s__ )
                                                    , rpc
                                                        { entity = Entity.Project.path
                                                        , decoder = Entity.Project.decode
                                                        , encode = Nothing
                                                        , kind = k_ "interests" projectId
                                                        , f = \( p_, s___ ) np -> ( ( p_, { s___ | projects = Dict.insert np.id np s__.projects } ), Toast.info "subscription" m_ )
                                                        , up = updateEntity
                                                        }
                                                    )
                                            , up = updateEntity
                                            }
                                        ]

                                _ ->
                                    Cmd.none

                        Entity.Owner ->
                            case t of
                                Session.Create _ ->
                                    rest
                                        { entity = Entity.Owner.path
                                        , kind = Api.Call.Rest.Create
                                        , decoder = Entity.Owner.decode
                                        , encode = Just <| Entity.Owner.encode <| Entity.Owner.fromForm <| Form.values s.owner
                                        , filter = Nothing
                                        , f =
                                            \( p, s__ ) o ->
                                                ( ( p
                                                  , { s__
                                                        | owners = Dict.insert o.id o s__.owners
                                                        , owner = Form.reset s__.owner
                                                    }
                                                  )
                                                , Cmd.batch
                                                    [ Toast.success "user-create" <| "Vytvořen nový uživatel, můžete se přihlásit"
                                                    , Browser.Navigation.pushUrl s__.key (Page.User.build Page.User.Details)
                                                    ]
                                                )
                                        , up = updateEntity
                                        }

                                Session.Delete id_ ->
                                    rest
                                        { entity = Entity.Owner.path
                                        , kind = Api.Call.Rest.Delete id_
                                        , decoder = Json.Decode.value
                                        , encode = Nothing
                                        , filter = Nothing
                                        , f =
                                            \( p, s__ ) _ ->
                                                ( ( p, { s__ | credentials = Nothing } )
                                                , Cmd.batch
                                                    [ Toast.info "project-delete" <| "Uživatel byl úspěšně smazán, včetně jeho projektů"
                                                    , Api.call s__ s__.ownersRequest
                                                    , Api.call s__ s__.projectsRequest
                                                    ]
                                                )
                                        , up = updateEntity
                                        }

                                Session.Login (Just _) ->
                                    rpc
                                        { entity = Entity.Owner.path
                                        , decoder = Entity.Owner.decode
                                        , encode = Nothing
                                        , kind = Api.Call.Procedure.Login
                                        , f =
                                            \( p, s__ ) o ->
                                                ( ( p
                                                  , { s__
                                                        | login = Form.reset s__.login
                                                        , credentials = Maybe.map (\c -> { c | id = Just o.id }) s__.credentials
                                                    }
                                                  )
                                                , Toast.success "login" <| "Vítejte, " ++ o.name
                                                )
                                        , up = updateEntity
                                        }

                                Session.Credentials ->
                                    (\e -> Component.Credentials.toBase64 e.email e.password) <| Entity.Owner.fromForm <| Form.values s.login

                                _ ->
                                    Cmd.none

                        Entity.Message ->
                            case t of
                                Session.ReadAll id_ ->
                                    rest
                                        { entity = Entity.Message.path
                                        , kind = Api.Call.Rest.GetAll
                                        , decoder = Entity.Message.decodeAll
                                        , encode = Just <| Entity.Message.encode <| Entity.Message.fromForm 1 <| Form.values s.message
                                        , filter = Just <| "projectId=" ++ String.fromInt id_
                                        , f =
                                            \( p, s__ ) ms ->
                                                ( ( p
                                                  , if Dict.isEmpty ms then
                                                        s__

                                                    else
                                                        { s__ | messages = Dict.insert id_ (Dict.values ms) s__.messages }
                                                  )
                                                , Cmd.none
                                                )
                                        , up = updateEntityAll
                                        }

                                Session.Create (Just id_) ->
                                    rest
                                        { entity = Entity.Message.path
                                        , kind = Api.Call.Rest.Create
                                        , decoder = Entity.Message.decode
                                        , encode = Just <| Entity.Message.encode <| Entity.Message.fromForm id_ <| Form.values s.message
                                        , filter = Nothing
                                        , f =
                                            \( p, s__ ) m ->
                                                ( ( p
                                                  , { s__
                                                        | messages =
                                                            if Dict.member m.projectId s__.messages then
                                                                Dict.update m.projectId (Maybe.map (List.append [ m ])) s__.messages

                                                            else
                                                                Dict.insert m.projectId [ m ] s__.messages
                                                        , message = Form.reset s__.message
                                                    }
                                                  )
                                                , Toast.success "message-create" <| "Zpráva byla vytvořena"
                                                )
                                        , up = updateEntity
                                        }

                                _ ->
                                    Cmd.none
            in
            ( s_, cmd )


updateEmpty msg f r =
    msg
        (\s ->
            case r of
                Ok _ ->
                    ( s, Cmd.none )

                Err e ->
                    ( s, Toast.error "err" e )
        )


updateEntity : ((Model msg -> ( Model msg, Cmd msg )) -> msg) -> (Model msg -> a -> ( Model msg, Cmd msg )) -> Result String a -> msg
updateEntity msg f r =
    msg
        (\( p, s ) ->
            case r of
                Ok e ->
                    f ( p, s ) e

                Err e ->
                    ( ( p, s ), Toast.error "err" e )
        )


updateEntityAll : ((Model msg -> ( Model msg, Cmd msg )) -> msg) -> (Model msg -> Dict Int { a | id : Int } -> ( Model msg, Cmd msg )) -> Result String (List { a | id : Int }) -> msg
updateEntityAll msg f r =
    msg
        (\( p, s ) ->
            case r of
                Ok es ->
                    f ( p, s ) (Dict.fromList <| List.map (\e -> ( e.id, e )) es)

                Err e ->
                    ( ( p, s ), Toast.error "err" e )
        )



--


load : (Session.Msg -> msg) -> ((Model msg -> ( Model msg, Cmd msg )) -> msg) -> ( Model msg, Cmd msg ) -> ( Model msg, Cmd msg )
load msgS msgU ( ( p, s ), cmd ) =
    case s.loaded of
        ( True, True ) ->
            case p of
                Page.Projects (Page.Projects.Detail (Page.Projects.Detail.Read id)) ->
                    if Dict.member id s.projects then
                        update msgS (Session.Request Entity.Message <| Session.ReadAll id) s msgU
                            |> (\( s_, cmd_ ) ->
                                    update msgS (Session.Request Entity.Project <| Session.View id) s_ msgU
                                        |> Tuple.mapSecond (Cmd.batch << (++) [ cmd, cmd_ ] << List.singleton)
                                        |> Tuple.mapFirst (\s__ -> ( p, s__ ))
                               )

                    else
                        ( ( p, s ), cmd )

                Page.Projects (Page.Projects.Detail (Page.Projects.Detail.Edit id)) ->
                    case Dict.get id s.projects of
                        Just p_ ->
                            ( ( p, { s | project = Form.updateAll (Entity.Project.toForm s.zone p_) s.project } ), Cmd.none )

                        Nothing ->
                            ( ( p, s ), cmd )

                _ ->
                    ( ( p, s ), cmd )

        _ ->
            ( ( p, s ), cmd )
