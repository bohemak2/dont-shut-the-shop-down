port module Component.Toast exposing
    ( Kind(..)
    , Model
    , add
    , decoder
    , error
    , info
    , putToast
    , remove
    , success
    , view
    )

import Html exposing (Html)
import Json.Decode
import Json.Decode.Extra
import Json.Encode


type Kind
    = Error
    | Success
    | Info


type alias Model =
    { id : String
    , kind : Kind
    , message : String
    , sleep : Int
    }


sleep : Int
sleep =
    3000


error : String -> String -> Cmd msg
error id msg =
    { id = id, kind = Error, message = msg, sleep = sleep }
        |> encode
        |> putToast


success : String -> String -> Cmd msg
success id msg =
    { id = id, kind = Success, message = msg, sleep = sleep }
        |> encode
        |> putToast


info : String -> String -> Cmd msg
info id msg =
    { id = id, kind = Info, message = msg, sleep = sleep }
        |> encode
        |> putToast


view : Model -> Html msg
view m =
    case m.kind of
        Error ->
            Html.text m.message

        Success ->
            Html.text m.message

        Info ->
            Html.text m.message


toString : Kind -> String
toString k =
    case k of
        Error ->
            "error"

        Success ->
            "success"

        Info ->
            "info"


fromString : String -> Kind
fromString s =
    case s of
        "error" ->
            Error

        "success" ->
            Success

        "info" ->
            Info

        _ ->
            Error


encode : Model -> Json.Encode.Value
encode m =
    Json.Encode.object
        [ ( "id", Json.Encode.string m.id )
        , ( "kind", Json.Encode.string <| toString m.kind )
        , ( "message", Json.Encode.string m.message )
        , ( "sleep", Json.Encode.int m.sleep )
        ]


add : (Maybe Model -> msg) -> Sub msg
add msg =
    addToast (msg << Result.toMaybe << Json.Decode.decodeValue decoder)


remove : (String -> msg) -> Sub msg
remove msg =
    removeToast msg


decoder : Json.Decode.Decoder Model
decoder =
    Json.Decode.succeed Model
        |> Json.Decode.Extra.andMap (Json.Decode.field "id" Json.Decode.string)
        |> Json.Decode.Extra.andMap (Json.Decode.field "kind" <| Json.Decode.map fromString Json.Decode.string)
        |> Json.Decode.Extra.andMap (Json.Decode.field "message" Json.Decode.string)
        |> Json.Decode.Extra.andMap (Json.Decode.field "sleep" Json.Decode.int)


port putToast : Json.Encode.Value -> Cmd msg


port addToast : (Json.Encode.Value -> msg) -> Sub msg


port removeToast : (String -> msg) -> Sub msg
