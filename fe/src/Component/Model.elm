module Component.Model exposing (Model)



import Component.Session.Model exposing (Session)
import Page


type alias Model msg =
    ( Page.Kind, Session msg )
