module Component.Credentials exposing
    ( Credentials
    , toBase64
    )

import Component.Session.Messages as Session
import Utils.Base64


type alias Credentials =
    { id : Maybe Int
    , hash : String
    }


toBase64 : String -> String -> Cmd msg
toBase64 email password =
    Utils.Base64.put ( Session.transformationToId Session.Base64Credentials, email ++ ":" ++ password )
