module Utils.String.Extra exposing (ellipsis)


ellipsis : Int -> String -> String
ellipsis max s =
    if 3 + String.length s > max then
        String.slice 0 max s ++ "..."

    else
        s
