module Utils.Time.Extra exposing
    ( fromInput
    , toInput
    , toString
    )

import Parser exposing ((|.), (|=), Parser)
import Time


toString : Time.Zone -> Time.Posix -> String
toString zone posix =
    let
        day =
            Time.toDay zone posix

        month =
            Time.toMonth zone posix

        year =
            Time.toYear zone posix
    in
    String.fromInt day
        ++ " "
        ++ monthString month
        ++ " "
        ++ String.fromInt year


toInput : Time.Zone -> Time.Posix -> String
toInput zone posix =
    let
        day =
            Time.toDay zone posix

        month =
            Time.toMonth zone posix

        year =
            Time.toYear zone posix

        go x =
            if String.length x == 1 then
                "0" ++ x

            else
                x
    in
    String.fromInt year
        ++ "-"
        ++ go (String.fromInt (monthInt month))
        ++ "-"
        ++ go (String.fromInt day)


fromInput : String -> Maybe Time.Posix
fromInput s =
    Parser.run parser s
        |> Result.toMaybe


daysM : Int -> Int -> Int
daysM m y =
    (case m of
        1 ->
            0

        2 ->
            31

        3 ->
            59

        4 ->
            90

        5 ->
            120

        6 ->
            151

        7 ->
            181

        8 ->
            212

        9 ->
            243

        10 ->
            273

        11 ->
            304

        12 ->
            334

        _ ->
            0
    )
        + (if modBy 4 y == 0 then
            1

           else
            0
          )


daysY : Int -> Int
daysY y =
    let
        ys =
            y - 1970
    in
    (ys * 365)
        + ((y - 1968 - 1) // 4)


msPerDay : Int
msPerDay =
    1000 * 60 * 60 * 24


days : Int -> Int -> Int -> Int
days y m d =
    d + daysM m y + daysY y


parser : Parser Time.Posix
parser =
    Parser.succeed (\y m d -> Time.millisToPosix (msPerDay * (days y m d - 1)))
        |= int
        |. Parser.symbol "-"
        |= int
        |. Parser.symbol "-"
        |= int


int : Parser Int
int =
    Parser.chompWhile Char.isDigit
        |> Parser.getChompedString
        |> Parser.andThen stringToInt


stringToInt : String -> Parser Int
stringToInt value =
    case String.toInt value of
        Nothing ->
            Parser.problem "Not an int"

        Just x ->
            Parser.succeed x


monthString : Time.Month -> String
monthString m =
    case m of
        Time.Jan ->
            "Leden"

        Time.Feb ->
            "Únor"

        Time.Mar ->
            "Březen"

        Time.Apr ->
            "Duben"

        Time.May ->
            "Květen"

        Time.Jun ->
            "Červen"

        Time.Jul ->
            "Červenec"

        Time.Aug ->
            "Srpen"

        Time.Sep ->
            "Září"

        Time.Oct ->
            "Říjen"

        Time.Nov ->
            "Listopad"

        Time.Dec ->
            "Prosinec"


monthInt : Time.Month -> Int
monthInt m =
    case m of
        Time.Jan ->
            1

        Time.Feb ->
            2

        Time.Mar ->
            3

        Time.Apr ->
            4

        Time.May ->
            5

        Time.Jun ->
            6

        Time.Jul ->
            7

        Time.Aug ->
            8

        Time.Sep ->
            9

        Time.Oct ->
            10

        Time.Nov ->
            11

        Time.Dec ->
            12
