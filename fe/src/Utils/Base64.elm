port module Utils.Base64 exposing (put, receive)


port put : ( Int, String ) -> Cmd msg


port receive : (( Int, String ) -> msg) -> Sub msg
