module Main exposing (init, main, subscriptions, update, view)

import Api
import Browser exposing (Document)
import Browser.Navigation
import Component.Model exposing (Model)
import Component.Session.Messages as Session
import Component.Session.Model as Session exposing (Flags, Htmls, Session)
import Component.Toast
import Dict
import Entity
import Entity.Notification
import Page
import Session
import Task
import Time
import Url
import Utils.Base64


type Msg
    = Msg Session.Msg
    | Cmd (Cmd Msg)
    | NoChange
    | Update (Model Msg -> ( Model Msg, Cmd Msg ))
    | UrlRequest Browser.UrlRequest
    | UrlChange Url.Url


main : Program Flags (Model Msg) Msg
main =
    Browser.application
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        , onUrlRequest = UrlRequest
        , onUrlChange = UrlChange
        }


init : Session.Flags -> Url.Url -> Browser.Navigation.Key -> ( Model Msg, Cmd Msg )
init flags url key =
    let
        p =
            Page.parse url

        s =
            Session.init Msg Cmd flags key url Update
    in
    ( ( p, s )
    , Cmd.batch
        [ Task.perform (\z -> Update (\( p_, s_ ) -> ( ( p_, { s_ | zone = z } ), Cmd.none ))) Time.here
        , Api.call s s.projectsRequest
        , Api.call s s.ownersRequest
        ]
    )


update : Msg -> Model Msg -> ( Model Msg, Cmd Msg )
update msg ( p, s ) =
    case msg of
        Msg msg_ ->
            Session.update Msg msg_ s Update |> Tuple.mapFirst (\s_ -> ( p, s_ ))

        Cmd cmd ->
            ( ( p, s ), cmd )

        NoChange ->
            ( ( p, s ), Cmd.none )

        Update f ->
            f ( p, s )

        UrlRequest rq ->
            case rq of
                Browser.Internal url ->
                    ( ( p, s ), Browser.Navigation.pushUrl s.key <| Url.toString url )

                Browser.External url ->
                    ( ( p, s ), Browser.Navigation.load url )

        UrlChange url ->
            let
                page =
                    Page.parse url
            in
            Session.load Msg Update ( ( page, s ), Cmd.none )


view : Model Msg -> { title : String, body : Htmls Msg }
view ( p, s ) =
    let
        ( title, body ) =
            Page.view p s
    in
    { title = title
    , body = body
    }


subscriptions : Model Msg -> Sub Msg
subscriptions _ =
    Sub.batch
        [ Utils.Base64.receive (Session.transform >> Maybe.map Msg >> Maybe.withDefault NoChange)
        , Component.Toast.add (Maybe.map (\t -> Update (\( p, s_ ) -> ( ( p, { s_ | toasts = Dict.insert t.id t s_.toasts } ), Cmd.none ))) >> Maybe.withDefault NoChange)
        , Component.Toast.remove (\tId -> Update (\( p, s_ ) -> ( ( p, { s_ | toasts = Dict.remove tId s_.toasts } ), Cmd.none )))
        , Entity.Notification.receiveInterests (\r -> Msg (Session.Request Entity.Project (Session.Subscription r)))
        ]
