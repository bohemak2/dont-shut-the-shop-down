module Page.Contact exposing
    ( build
    , parser
    , view
    )

import Component.Session.Model exposing (Htmls, PageResponse)
import Html exposing (Html)
import Html.Attributes
import Url.Builder
import Url.Parser


url : String
url =
    "kontakt"


parser : Url.Parser.Parser a a
parser =
    Url.Parser.s url


build : String
build =
    Url.Builder.absolute [ url ] []


title : String
title =
    "Kontakt"


view : PageResponse msg
view =
    ( title
    , [ Html.h1 [] [ Html.text title ] ] ++ viewContact
    )


viewContact : Htmls msg
viewContact =
    [ Html.section []
        [ Html.text "doc. Mgr. Tomáš Kozel, Ph.D. - "
        , Html.a [ Html.Attributes.href "mailto:tomas.kozel@uhk.cz" ] [ Html.text "tomas.kozel@uhk.cz" ]
        ]
    , Html.section []
        [ Html.text "Bc. Lukáš Richtrmoc - "
        , Html.a [ Html.Attributes.href "mailto:lukas.richtrmoc@uhk.cz" ] [ Html.text "lukas.richtrmoc@uhk.cz" ]
        , Html.text ", "
        , Html.a [ Html.Attributes.href "mailto:bohemak2@seznam.cz" ] [ Html.text "bohemak2@seznam.cz" ]
        ]
    ]
