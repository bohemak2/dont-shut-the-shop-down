module Page.NotFound exposing (view)

import Component.Session.Model exposing (PageResponse)
import Html exposing (Html)


title : String
title =
    "404"


view : PageResponse msg
view =
    ( title, [ Html.h1 [] [ Html.text title ] ] )
