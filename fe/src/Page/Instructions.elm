module Page.Instructions exposing (build, parser, view)

import Component.Session.Model exposing (Htmls, PageResponse)
import Component.Theme as Theme
import Html exposing (Html)
import Url.Builder
import Url.Parser


url : String
url =
    "navod"


parser : Url.Parser.Parser a a
parser =
    Url.Parser.s url


build : String
build =
    Url.Builder.absolute [ url ] []


title : String
title =
    "Návod"


view : PageResponse msg
view =
    ( title, [ Html.h1 [] [ Html.text title ] ] ++ viewBody )


viewBody : Htmls msg
viewBody =
    [ viewReasons
    , viewInstructions
    , viewCookies
    , viewPersonal
    ]


viewReasons : Html msg
viewReasons =
    Html.section Theme.centerText
        [ Html.h1 [] [ Html.text "Důvody" ]
        , Html.p [] [ Html.text "Aplikace pro prezentaci projektů vznikla na pozadí virové pandemie jako jednoduchý prostředek pro šíření altruismu potřebným." ]
        ]


viewInstructions : Html msg
viewInstructions =
    Html.section []
        [ Html.h1 Theme.centerText [ Html.text "Instrukce" ]
        , Html.ol []
            [ Html.li [] [ Html.text "Registrace uživatele" ]
            , Html.li [] [ Html.text "Přihlášení" ]
            , Html.li [] [ Html.text "Vytvoření projektu (max 3 na osobu)" ]
            , Html.li [] [ Html.text "Rozesílání zpráv odběratelům (max 1 za 2 dny)" ]
            ]
        ]


viewCookies : Html msg
viewCookies =
    Html.section Theme.centerText
        [ Html.h1 [] [ Html.text "Cookies" ]
        , Html.p [] [ Html.text "Stránka neshromažďuje údaje o uživateli kromě ID odebíraných projektů." ]
        ]


viewPersonal : Html msg
viewPersonal =
    Html.section Theme.centerText
        [ Html.h1 [] [ Html.text "Zásady zpracování osobních údajů" ]
        , Html.p [] [ Html.text "Identifikační údaje uložené v systému po registraci: IČ, jméno, email. V případě odběru notifikací: klíče prohlížeče. K zániku dat (včetně všech přidružených) dochází při smázání osoby." ]
        ]
