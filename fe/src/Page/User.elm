module Page.User exposing
    ( Kind(..)
    , build
    , parser
    , title
    , url
    , view
    , viewCredentials
    )

import Component.Credentials exposing (Credentials)
import Component.Form as Form
import Component.Session.Messages as Session
import Component.Session.Model exposing (Extended, PageResponse, Session)
import Component.Theme as Theme
import Dict
import Entity
import Entity.Owner
import Html exposing (Html)
import Html.Events
import Url.Builder
import Url.Parser exposing ((</>))
import Utils.Time.Extra


type Kind
    = Details
    | Registration


url : String
url =
    "uzivatel"


parser : Url.Parser.Parser (Kind -> a) a
parser =
    let
        pathBasic =
            Url.Parser.s url
    in
    Url.Parser.oneOf
        [ Url.Parser.map Details pathBasic
        , Url.Parser.map Registration (pathBasic </> Url.Parser.s "registrace")
        ]


build : Kind -> String
build k =
    let
        path =
            case k of
                Details ->
                    []

                Registration ->
                    [ "registrace" ]
    in
    Url.Builder.absolute ([ url ] ++ path) []


title : Kind -> String
title k =
    case k of
        Details ->
            "Uživatel"

        Registration ->
            "Registrace"


view : Session msg -> Kind -> PageResponse msg
view s k =
    let
        name =
            title k
    in
    ( name
    , [ Html.h1 [] [ Html.text name ]
      , case k of
            Details ->
                viewLogin s

            Registration ->
                viewRegistration s
      ]
    )


viewLogin : Session msg -> Html msg
viewLogin s =
    case ( s.credentials, s.credentials |> Maybe.andThen .id ) of
        ( Just c, Just _ ) ->
            viewCredentials s c True

        _ ->
            viewLoginForm s


viewLoginForm : Session msg -> Html msg
viewLoginForm s =
    Form.view
        (Session.Form Entity.Owner <| Session.Login Nothing)
        (Session.Request Entity.Owner Session.Credentials)
        s.login
        |> Html.map s.msg


viewCredentials : Session msg -> Credentials -> Extended -> Html msg
viewCredentials s c e =
    case Maybe.andThen (\id -> Dict.get id s.owners) c.id of
        Just o ->
            if e then
                Html.ul Theme.column
                    [ Html.li [] [ Html.text "Jméno: ", Html.b [] [ Html.text o.name ] ]
                    , Html.li [] [ Html.text "Registrován: ", Html.b [] [ Html.text <| Utils.Time.Extra.toString s.zone o.created ] ]
                    , Html.li [] [ Html.text "Email: ", Html.b [] [ Html.text o.email ] ]
                    , Html.li [] [ Html.text "IČO: ", Html.b [] [ Html.text o.vat ] ]
                    , Html.li [] [ viewDelete s o ]
                    ]

            else
                Html.text o.name

        Nothing ->
            Html.text "Uživatel nenalezen"


viewDelete : Session msg -> Entity.Owner.Model -> Html msg
viewDelete s o =
    Html.div (Theme.column ++ Theme.centerY)
        [ Html.button
            (Theme.button
                ++ [ Html.Events.onClick (s.msg <| Session.Request Entity.Owner <| Session.Delete o.id)
                   , Theme.style "margin-top" "4em"
                   ]
            )
            [ Html.text "Smazat uživatele" ]
        , Html.small [] [ Html.text "Smažou se i projekty, zprávy a odběry!" ]
        ]


viewRegistration : Session msg -> Html msg
viewRegistration s =
    Form.view
        (Session.Form Entity.Owner (Session.Create Nothing))
        (Session.Request Entity.Owner (Session.Create Nothing))
        s.owner
        |> Html.map s.msg
