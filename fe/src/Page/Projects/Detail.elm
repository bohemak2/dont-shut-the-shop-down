module Page.Projects.Detail exposing
    ( Kind(..)
    , build
    , parser
    , view
    )

import Component.Form as Form
import Component.Session.Messages as Session
import Component.Session.Model exposing (Htmls, PageResponse, Session)
import Component.Theme as Theme
import Dict
import Entity
import Entity.Message
import Entity.Project
import Html exposing (Html)
import Html.Attributes
import Html.Events
import Page.Projects.Donation
import Time
import Url.Builder
import Url.Parser exposing ((</>))
import Utils.Time.Extra


type Kind
    = Create
    | Edit Int
    | Read Int
    | Donation Int Int


parser : Url.Parser.Parser (Kind -> a) a
parser =
    Url.Parser.oneOf
        [ Url.Parser.map Read Url.Parser.int
        , Url.Parser.map Edit (Url.Parser.int </> Url.Parser.s "edit")
        , Url.Parser.map Create (Url.Parser.s "novy")
        , Url.Parser.map Donation (Url.Parser.int </> Page.Projects.Donation.parser)
        ]


build : List String -> Kind -> String
build before kind =
    let
        path =
            case kind of
                Create ->
                    [ "novy" ]

                Read id ->
                    [ String.fromInt id ]

                Edit id ->
                    [ String.fromInt id, "edit" ]

                Donation id value ->
                    [ String.fromInt id ] ++ Page.Projects.Donation.build value
    in
    Url.Builder.absolute (before ++ path) []


title : Session msg -> Kind -> String
title s kind =
    let
        go id =
            getProject s id
                |> Maybe.map .name
                |> Maybe.withDefault (String.fromInt id)
    in
    case kind of
        Create ->
            "Nový projekt"

        Edit id ->
            "Editace " ++ go id

        Read id ->
            go id

        Donation id v ->
            go id ++ " - dar " ++ String.fromInt v


view : Session msg -> Kind -> (Kind -> String) -> PageResponse msg
view s kind buildF =
    ( title s kind, viewDetail s kind buildF )


viewDetail : Session msg -> Kind -> (Kind -> String) -> Htmls msg
viewDetail s kind buildF =
    case kind of
        Create ->
            [ Html.h1 [] [ Html.text "Nový projekt" ]
            , viewFormCreate s
            ]

        Edit id ->
            case getProject s id of
                Just p ->
                    [ Html.h1 [] [ Html.text "Editace projektu" ]
                    , viewFormEdit s p
                    ]

                Nothing ->
                    viewProject s Nothing buildF

        Read id ->
            viewRead s id buildF

        Donation id value ->
            [ Html.div
                (Theme.toasts
                    ++ [ Theme.style "top" Theme.space
                       , Theme.style "padding" Theme.space
                       ]
                )
                [ Html.div
                    (Theme.toast
                        ++ [ Theme.style "padding" ".5em 1em" ]
                    )
                    [ Html.span []
                        [ Html.text "Kliknutím "
                        , Html.a [ Html.Attributes.href <| buildF <| Read id ] [ Html.text "zde" ]
                        , Html.text " zavřete okno"
                        ]
                    , Html.hr [] []
                    , Page.Projects.Donation.view s id value
                    ]
                ]
            ]
                ++ viewRead s id buildF


viewRead : Session msg -> Entity.Project.ProjectId -> (Kind -> String) -> Htmls msg
viewRead s id buildF =
    case getProject s id of
        Just p ->
            viewProject s (Just p) buildF
                ++ [ viewMessages s p ]
                ++ viewEditation s p buildF

        Nothing ->
            viewProject s Nothing buildF


viewEditation : Session msg -> Entity.Project.Model -> (Kind -> String) -> Htmls msg
viewEditation s p buildF =
    if isOwner s p then
        [ Html.h2 [] [ Html.text "Editace" ]
        , Html.span [] [ Html.b [] [ Html.text "Přidat zprávu" ] ]
        ]
            ++ viewFormMessage s p
            ++ [ Html.p [] [ Html.b [] [ Html.text "Změnit projekt" ] ] ]
            ++ viewEditButton s p buildF
            ++ viewDeleteButton s p

    else
        []


viewDeleteButton : Session msg -> Entity.Project.Model -> Htmls msg
viewDeleteButton s p =
    if isOwner s p then
        [ Html.button
            (Theme.button ++ [ Html.Events.onClick (s.msg <| Session.Request Entity.Project <| Session.Delete p.id) ])
            [ Html.text "Smazat projekt" ]
        ]

    else
        []


viewEditButton : Session msg -> Entity.Project.Model -> (Kind -> String) -> Htmls msg
viewEditButton s p buildF =
    if isOwner s p then
        [ Html.a
            (Theme.button ++ [ Html.Attributes.href (buildF <| Edit p.id) ])
            [ Html.text "Editovat projekt" ]
        ]

    else
        []


viewFormCreate : Session msg -> Html msg
viewFormCreate s =
    case s.credentials of
        Just c ->
            Form.view
                (Session.Form Entity.Project <| Session.Create c.id)
                (Session.Request Entity.Project <| Session.Create c.id)
                s.project
                |> Html.map s.msg

        Nothing ->
            Html.text "K vytvoření projektu je nutné se přihlásit."


viewFormEdit : Session msg -> Entity.Project.Model -> Html msg
viewFormEdit s p =
    if isOwner s p then
        Form.view
            (Session.Form Entity.Project <| Session.Update p.id)
            (Session.Request Entity.Project <| Session.Update p.id)
            s.project
            |> Html.map s.msg

    else
        Html.text "Editovat lze pouze vaše projekty"


viewFormMessage : Session msg -> Entity.Project.Model -> Htmls msg
viewFormMessage s p =
    if isOwner s p then
        [ Form.view
            (Session.Form Entity.Message <| Session.Create (Just p.id))
            (Session.Request Entity.Message <| Session.Create (Just p.id))
            s.message
            |> Html.map s.msg
        ]

    else
        []


viewProject : Session msg -> Maybe Entity.Project.Model -> (Kind -> String) -> Htmls msg
viewProject s p buildF =
    case p of
        -- TODO: It should be handled in Main.urlChange
        Nothing ->
            [ Html.h1 [] [ Html.text "Je nám líto, projekt již neexistuje." ] ]

        Just p_ ->
            viewProjectHeader s p_
                ++ viewProjectBody s p_
                ++ viewProjectPrices s p_ buildF


viewProjectHeader : Session msg -> Entity.Project.Model -> Htmls msg
viewProjectHeader s p =
    [ Html.h1 [] [ Html.text p.name ]
    , Html.span []
        [ Html.text "Autor: "
        , Html.b [] [ Html.text <| Maybe.withDefault (String.fromInt p.ownerId) <| Maybe.map .name <| Dict.get p.ownerId s.owners ]
        ]
    , Html.span []
        [ viewTime s p.created
        , Html.text " -  "
        , viewTime s p.until
        ]
    , Html.span [] (viewViews p)
    ]


viewProjectBody : Session msg -> Entity.Project.Model -> Htmls msg
viewProjectBody s p =
    [ Html.p Theme.centerText [ Html.b [] [ Html.text p.summary ] ] ]


viewProjectPrices : Session msg -> Entity.Project.Model -> (Kind -> String) -> Htmls msg
viewProjectPrices s p buildF =
    [ Html.h2 [] [ Html.text "Přispět" ]
    ]
        ++ viewPrices p buildF


viewTime : Session msg -> Time.Posix -> Html msg
viewTime s t =
    Html.time [ Html.Attributes.datetime <| Utils.Time.Extra.toInput s.zone t ] [ Html.text <| Utils.Time.Extra.toString s.zone t ]


viewViews : Entity.Project.Model -> Htmls msg
viewViews p =
    case p.views of
        Just i ->
            [ Html.text <| String.fromInt i
            , Html.text "x shlédnuto, "
            , viewInterests p
            ]

        Nothing ->
            []


viewPrices : Entity.Project.Model -> (Kind -> String) -> Htmls msg
viewPrices p buildF =
    if p.price1 > 0 || p.price2 > 0 then
        let
            a i =
                if i > 0 then
                    [ Html.a
                        (Theme.button
                            ++ [ Html.Attributes.href (buildF <| Donation p.id i) ]
                        )
                        [ Html.text <| String.fromInt i ]
                    ]

                else
                    []
        in
        [ Html.ul (Theme.row ++ Theme.centerX) (a p.price1 ++ a p.price2)
        ]

    else
        [ Html.text "Projekt nevypsal žádné dary" ]



--


viewMessages : Session msg -> Entity.Project.Model -> Html msg
viewMessages s p =
    Html.div (Theme.column ++ Theme.centerY)
        ([ Html.h2 [] [ Html.text "Zprávy" ]
         , viewInterestsButton s p
         ]
            ++ (case Dict.get p.id s.messages of
                    Just ms ->
                        [ Html.ul Theme.column (List.map (\m -> Html.li [] [ viewMessage s m ]) ms) ]

                    Nothing ->
                        [ Html.p [] [ Html.text "Žádné zprávy" ]
                        ]
               )
        )


viewInterests : Entity.Project.Model -> Html msg
viewInterests p =
    let
        v =
            case p.interests of
                Just i ->
                    i

                Nothing ->
                    0
    in
    Html.text (String.fromInt v ++ "x odběr")


viewInterestsButton : Session msg -> Entity.Project.Model -> Html msg
viewInterestsButton s p =
    let
        a =
            Theme.button
                ++ [ Html.Events.onClick (s.msg <| Session.Request Entity.Project <| Session.Interest p.id) ]

        t =
            case List.member p.id s.flags of
                True ->
                    "Zrušit odběr"

                False ->
                    "Odebírat zprávy"
    in
    Html.button a [ Html.text t ]


viewMessage : Session msg -> Entity.Message.Model -> Html msg
viewMessage s ms =
    Html.article []
        [ Html.h1 [] [ Html.text <| Utils.Time.Extra.toString s.zone ms.created ]
        , Html.text ms.text
        ]



--


getProject : Session msg -> Entity.Project.ProjectId -> Maybe Entity.Project.Model
getProject s id =
    Dict.get id s.projects


isOwner : Session msg -> Entity.Project.Model -> Bool
isOwner s p =
    case s.credentials |> Maybe.andThen .id |> Maybe.andThen (\id -> Dict.get id s.owners) of
        Just o ->
            o.id == p.ownerId

        Nothing ->
            False
