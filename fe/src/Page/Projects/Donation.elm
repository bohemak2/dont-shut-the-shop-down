module Page.Projects.Donation exposing (build, parser, url, view)

import Component.Session.Model exposing (Session)
import Dict
import Entity.Owner
import Entity.Project
import Html exposing (Html)
import Html.Attributes
import Url.Parser exposing ((</>))


url : String
url =
    "dar"


parser : Url.Parser.Parser (Int -> a) a
parser =
    Url.Parser.s url </> Url.Parser.int


build : Entity.Project.Price -> List String
build id =
    [ url, String.fromInt id ]


view : Session msg -> Entity.Project.ProjectId -> Entity.Project.Price -> Html msg
view s id i =
    case Dict.get id s.projects of
        Just p ->
            Html.section []
                [ Html.h1 [] [ Html.text <| "Dar pro: " ++ p.name ]
                , viewTrust p (Dict.get p.ownerId s.owners)
                , Html.p []
                    [ Html.text <| "Bankovní účet: " ++ p.bankAccount
                    , Html.text " / "
                    , Html.text p.bankCode
                    ]
                , Html.p [] [ Html.text <| "Částka: " ++ String.fromInt i ]
                , Html.hr [] []
                , viewQr p i
                ]

        Nothing ->
            Html.section []
                [ Html.h1 [] [ Html.text "Neznámý projekt, není možné darovat peníze" ] ]


viewTrust : Entity.Project.Model -> Maybe Entity.Owner.Model -> Html msg
viewTrust p o =
    case ( o, p.bankTrust ) of
        ( Just o_, Just v ) ->
            if v then
                Html.p [] [ Html.text <| "Účet byl ověřen, vlastníkem je " ++ o_.name ++ "." ]

            else
                Html.p []
                    [ Html.text "Účet nemusí být pravý, vlastníkem dodaný "
                    , Html.a [ Html.Attributes.target "_blank", Html.Attributes.href p.bankTrustUrl ] [ Html.text "zdroj" ]
                    , Html.text " nemá dostatečné oprávnění."
                    ]

        ( Just o_, Nothing ) ->
            Html.p []
                [ Html.text <| "Účet nebyl ověřen, jeho vlastník '" ++ o_.name ++ "' připojuje pravost účtu na tomto "
                , Html.a [ Html.Attributes.target "_blank", Html.Attributes.href p.bankTrustUrl ] [ Html.text "zdroji" ]
                , Html.text "."
                ]

        _ ->
            Html.text "Účet nebyl ověřen nebo jeho vlastník již není v systému."


viewQr : Entity.Project.Model -> Entity.Project.Price -> Html msg
viewQr p i =
    Html.img
        [ Html.Attributes.src <| qr p i
        , Html.Attributes.alt "Pokud nevidíte QR obrázek, jste bez internetu, nebo bankovní účet neprošel externí kontrolou."
        ]
        []


qr : Entity.Project.Model -> Entity.Project.Price -> String
qr p i =
    "http://api.paylibo.com/paylibo/generator/czech/image?accountNumber=" ++ p.bankAccount ++ "&bankCode=" ++ p.bankCode ++ "&amount=" ++ String.fromInt i ++ "&currency=CZK&vs=&message=Donation&size=200"
