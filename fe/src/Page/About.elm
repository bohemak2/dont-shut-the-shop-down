module Page.About exposing
    ( build
    , parser
    , view
    )

import Component.Session.Model exposing (Htmls, PageResponse)
import Component.Theme as Theme
import Html exposing (Html)
import Html.Attributes
import Url.Builder
import Url.Parser


url : String
url =
    "o-nas"


parser : Url.Parser.Parser a a
parser =
    Url.Parser.s url


build : String
build =
    Url.Builder.absolute [ url ] []


title : String
title =
    "O nás"


view : PageResponse msg
view =
    ( title
    , [ Html.h1 [] [ Html.text title ] ] ++ viewCredentials
    )


viewCredentials : Htmls msg
viewCredentials =
    [ Html.p Theme.centerText
        [ Html.text "Webovou aplikaci napsal Lukáš Richtrmoc v rámci diplomové práce na "
        , Html.a
            [ Html.Attributes.href "https://www.uhk.cz/cs/fakulta-informatiky-a-managementu"
            , Html.Attributes.target "_blank"
            ]
            [ Html.text "Univerzitě Hradec Králové" ]
        , Html.text ". Využívá Elm 0.19, json-server a web-push. Není povoleno ke komerčnímu užití."
        ]
    , Html.p []
        [ Html.a
            [ Html.Attributes.href "https://bitbucket.org/bohemak2/dont-shut-the-shop-down/"
            , Html.Attributes.target "_blank"
            ]
            [ Html.text "bitbucket.org/bohemak2/dont-shut-the-shop-down/" ]
        ]
    ]
