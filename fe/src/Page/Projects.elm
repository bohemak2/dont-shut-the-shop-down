module Page.Projects exposing
    ( Kind(..)
    , build
    , parser
    , view
    , viewAwareness
    )

import Component.Session.Model exposing (Htmls, PageResponse, Session)
import Component.Theme as Theme
import Dict
import Entity.Project
import Html exposing (Html)
import Html.Attributes
import Page.Projects.Detail
import Time
import Url.Builder
import Url.Parser exposing ((</>))
import Utils.String.Extra


type Kind
    = All
    | Detail Page.Projects.Detail.Kind


url : String
url =
    "projekty"


parser : Url.Parser.Parser (Kind -> a) a
parser =
    let
        pathBasic =
            Url.Parser.s url
    in
    Url.Parser.oneOf
        [ Url.Parser.map All pathBasic
        , Url.Parser.map Detail (pathBasic </> Page.Projects.Detail.parser)
        ]


build : Kind -> String
build k =
    case k of
        All ->
            Url.Builder.absolute [ url ] []

        Detail kd ->
            Page.Projects.Detail.build [ url ] kd


title : String
title =
    "Projekty"


view : Session msg -> Kind -> PageResponse msg
view session kind =
    case kind of
        All ->
            ( title, viewProjects session )

        Detail k ->
            Page.Projects.Detail.view session k (build << Detail)


viewProjects : Session msg -> Htmls msg
viewProjects session =
    [ Html.h1 [] [ Html.text title ]
    , Html.div (Theme.row ++ Theme.spacingY ++ Theme.centerX) (List.map viewProject <| List.sortWith sort <| Dict.values session.projects)
    ]
        ++ (if Dict.isEmpty session.projects then
                []

            else
                [ viewAwareness ]
           )
        ++ [ Html.a
                (Theme.button ++ [ Html.Attributes.href (build <| Detail Page.Projects.Detail.Create) ])
                [ Html.text "Přidat projekt" ]
           ]


viewProject : Entity.Project.Model -> Html msg
viewProject p =
    Html.article (Theme.column ++ Theme.spacing ++ Theme.centerY ++ Theme.button ++ [ Theme.style "min-width" "160px", Theme.style "max-width" "240px", Theme.style "cursor" "auto" ])
        [ Html.h1 Theme.centerText [ Html.a [ Html.Attributes.href (build (Detail <| Page.Projects.Detail.Read p.id)) ] [ Html.text p.name ] ]
        , Html.p (Theme.centerText ++ [ Theme.style "margin-top" Theme.space ]) [ Html.text <| Utils.String.Extra.ellipsis 60 <| p.summary ]
        ]


sort : Entity.Project.Model -> Entity.Project.Model -> Order
sort a b =
    let
        a_ =
            Time.posixToMillis a.created

        b_ =
            Time.posixToMillis b.created
    in
    case compare a_ b_ of
        LT ->
            GT

        EQ ->
            EQ

        GT ->
            LT



--


viewAwareness : Html msg
viewAwareness =
    Html.p []
        [ Html.small [] [ Html.text "(Veškeré projekty jsou smyšlené, nepřispívejte!)" ] ]
