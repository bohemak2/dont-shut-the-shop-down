module Page.Home exposing
    ( build
    , parser
    , view
    )

import Component.Session.Model exposing (PageResponse)
import Component.Theme as Theme
import Html exposing (Html)
import Html.Attributes
import Page.Projects
import Page.Projects.Detail
import Url.Builder
import Url.Parser


parser : Url.Parser.Parser a a
parser =
    Url.Parser.top


build : String
build =
    Url.Builder.absolute [] []


title : String
title =
    "Vítejte na projektovači"


view : PageResponse msg
view =
    ( title
    , [ Html.h1 [] [ Html.text title ]
      , viewDescription
      , Html.a (Theme.button ++ [ Html.Attributes.href (Page.Projects.build <| Page.Projects.Detail Page.Projects.Detail.Create) ]) [ Html.text "Založit projekt" ]
      , Html.a (Theme.spacingY ++ [ Html.Attributes.href (Page.Projects.build Page.Projects.All) ]) [ Html.text "Prohlédnout projekty" ]
      , Page.Projects.viewAwareness
      ]
    )


viewDescription : Html msg
viewDescription =
    Html.p []
        [ Html.text "Znáte projekt, který chcete podpořit? Máte vlastní projekt? Tak jste tu správně."
        ]
