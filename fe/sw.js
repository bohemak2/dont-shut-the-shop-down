self.addEventListener('push', event => {
    const data = event.data.json();

    const options = {
        body: data.body,
        icon: '/assets/favicon.png',
        data: {
            click_url: '/projekty/'+ data.id
        }
    };

    self.registration.showNotification(data.title, options);
});