// server.js use json-server based on express.js
// PATH are relative to position of script (nowadays /be/)
const fs = require("fs");
const jsonServer = require('json-server');
const request = require('request');
const crypto = require('crypto');
const wp = require('web-push');
const keys = getKeys('./key/vapid');

const server = jsonServer.create();
const router = jsonServer.router('./db/db.json');
const config = require('./db/config.json');
const scheme = makeRules(require('./db/scheme.json'));

const isDev = process.argv.includes("dev");
const port = isDev ? 8094 : 8080;
const staticPath = isDev ? "../fe/" : "./public/";

/* INIT */
server.use(jsonServer.defaults({static: staticPath}));
server.use(jsonServer.bodyParser);

wp.setVapidDetails(
    'mailto:bohemak2@gmail.com',
    keys.publicKey,
    keys.privateKey
);


/* FLOW
* url: config.prefix
* version: config.version
* type: config.rest (GET, POST, PATCH, DELETE), config.rpc (GET)
* entity: scheme.json
* id: entity id
* procedure: rpc procedure for call
* */
server.use((req, res, next) => {
        const [_, url, version, type, entity, id, procedure] = req.path.split("/");

        if (url === config.prefix && version === config.version) {
            if (entity in scheme) {

                if (req.method === "GET" || req.method === "POST") {
                    if (type === config.rpc) {
                        completeProcedure(req, entity, id, procedure)
                            .then(r => {
                                res.status(200).jsonp(r);
                            })
                            .catch(e => setResponse(res, e.c !== undefined ? e.c : 406, e.t !== undefined ? e.t : "Neznámá chyba"));
                        server.use('/' + [config.prefix, config.version, type].join("/"), router);
                        return false;
                    }
                }

                if (config.protect.includes(entity)){
                    setResponse(res, 406, entity + " je chráněn");
                } else {
                    if (req.method === "POST" || req.method === "PUT" || req.method === "PATCH" || req.method === "DELETE") {
                        if (type === config.rest) {
                            isAuthorized(req, entity)
                                .then(user => itsObject(req, entity, id, user))
                                .then(user => hasLimits(req, entity, id, user))
                                .then(user => completeBody(req, entity, id, user))
                                .then(user => notificate(req, entity, id, user).then(() => {
                                    next();
                                }))
                                .catch(e => setResponse(res, e.c !== undefined ? e.c : 406, e.t !== undefined ? e.t : "Neznámá chyba"));
                            server.use('/' + [config.prefix, config.version, type].join("/"), router);
                            return false;
                        }
                    }

                    next();
                    server.use('/' + [config.prefix, config.version, type].join("/"), router);
                }
            } else {
                setResponse(res, 404, "Neznámý dotaz.");
            }
        } else {
            // Not API? Forward on static server
            req.pipe(request({qs: req.query, uri: "http://localhost:" + port})).pipe(res);
        }
    }
);

server.listen(port, () => {
    console.log('JSON Server is running on ' + port);
})

function setResponse(res, code, msg) {
    res.status(code).jsonp({
        msg: msg
    });
}


/* *
* OAuth - Authorization: Basic base64(email:pwd)
* */
function isAuthorized(r, e) {
    const auth = r.headers["authorization"];

    if (r.method === "POST" && e === config.users) {
        return Promise.resolve();
    } else if (auth !== undefined) {
        const [type, base64] = auth.split(" ");
        const [email, password] = Buffer.from(base64, "base64").toString().split(":");

        return createHash(type, password).then((p => {
                const u = router.db.get(config.users).filter({
                    "email": email,
                    "password": p
                }).value();
                return u.length === 1 ? Promise.resolve(u[0]) : Promise.reject(Error("Přihlašovací údaje nesedí", 401));
            }
        ));
    } else {
        return Promise.reject(Error("Nejsou vyplněny přihlašovací údaje", 401))
    }
}

/* Is it author's object? */
function itsObject(r, e, id, u) {
    if (r.method !== "POST") {
        if (router.db.get(e).getById(id).value() === undefined) {
            return Promise.reject(Error(e + " s id " + id + " neexistuje ", 404));
        }

        const rule = scheme[e].r["ownerId"];

        if (rule !== undefined) {
            const o = router.db.get(e).filter({"ownerId": u.id}).value();

            if ((o[0] === undefined) || (o[0].ownerId !== u.id)) {
                return Promise.reject(Error(u.id + " není vlastník " + e, 405));
            }
        }

        if (e === "message") {
            const p = router.db.get("project").getById(r.body["projectId"]).value();

            if ((p === undefined) || (p.ownerId !== u.id)) {
                return Promise.reject(Error(u.id + " není vlastník projektu " + p.id, 405));
            }
        }
    }

    return Promise.resolve(u);
}

/* Owner can have 3 projects and 1 message per 2 days */
function hasLimits(r, e, id, u) {
    if (r.method === "POST") {

        if (e === "project") {
            const ps = router.db.get(e).filter({"ownerId": u.id}).value();

            if (ps.length >= 3) {
                return Promise.reject(Error("Vyčerpáno možných projektů (3 celkem)", 405));
            }
        }

        if (e === "message") {
            const p = router.db.get("project").getById(r.body["projectId"]).value();

            if (p !== undefined) {
                const m = router.db.get("message").filter({"projectId": p.id}).sortBy("created").reverse().take(1).value();
                const d = posix() - 2 * 24 * 60 * 60 * 1000

                if (m[0] !== undefined && m[0].created > d) {
                    return Promise.reject(Error("Pouze 1 zpráva za 2 dny", 405));
                }
            } else {
                return Promise.reject(Error("Projekt neexistuje", 406));
            }
        }
    }

    return Promise.resolve(u);
}

/* *
/* REST API
* */

// DROP rows not for UPDATE (PATCH), CANCEL if something is missing for CREATE (POST), DELETE with CASCADING
function completeBody(r, e, id, user) {
    if (r.method === "PATCH") {
        if (r.body !== undefined && Object.entries(r.body).length > 0) {
            const promises = [];

            for (const a in r.body) {
                const rule = scheme[e].r[a];
                if (scheme[e].f[a] !== undefined && rule !== "ignore" && rule !== "int$posixOnCreate" && a !== "procedure") {
                    promises.push(scheme[e].f[a](a, r.body[a], e));
                } else {
                    delete r.body[a];
                }
            }

            return Promise.all(promises);
        } else {
            return Promise.reject(Error("V " + e + " chybí položky"));
        }
    } else if (r.method === "POST") {
        if (r.body !== undefined && Object.entries(r.body).length > 0) {
            const promises = [];

            for (const a in r.body) {
                if (scheme[e].f[a] === undefined && scheme[e].r[a] !== "ignore" && a !== "procedure") {
                    delete r.body[a];
                }
            }

            for (const a in scheme[e].f) {
                const rule = scheme[e].r[a];

                if (r.body[a] === undefined && rule !== "ignore" && a !== "procedure" && rule !== "int$posixOnCreate") {
                    promises.push(Promise.reject(Error(e + " neobsahuje položku: " + a)));
                } else {
                    promises.push(scheme[e].f[a](a, r.body[a], e).then(p => {
                        r.body[a] = p;
                    }));
                }
            }
            return Promise.all(promises);
        } else {
            return Promise.reject(Error("V " + e + " chybí položky"));
        }
    } else if (r.method === "DELETE") {

        const go = function (e1, id1) {
            // Remove all relatives (original id will be removed itself by json-server)
            const asFk = e1 + "Id";
            for (const e2 in scheme) {
                if (asFk in scheme[e2].r) {
                    const f = {};
                    f[asFk] = id1;
                    const ids = router.db.get(e2).filter(f).value();
                    if (ids.length > 0) {
                        for (const id2 of ids) {
                            router.db.get(e2).remove({id: id2.id}).write();
                            go(e2, id2.id);
                        }
                    }
                }
            }
        }

        if (id !== undefined && config.cascading.includes(e)) {
            go(e, parseInt(id));
        }
    }
    return Promise.resolve(user);
}

/* *
/* Handle Web Push notifications
* */

// If new Message, send to all interested clients
function notificate(r, e, id, user) {
    if (r.method === "POST") {
        if (e === "message") {
            const pId = r.body["projectId"];
            const p = router.db.get("project").getById(pId).value();

            if (p !== undefined) {
                const ns = router.db.get(config.notifications).filter({"projectId": pId}).value();

                for (const n of ns) {
                    const payload = JSON.stringify({
                        title: p.name,
                        body:  r.body["text"].substr(0, r.body["text"].length < 100 ? r.body["text"].length : 100) || "Nová zpráva",
                        entity: e,
                        id: p.id
                    });

                    wp.sendNotification(n, payload)
                        .catch(e => e /* Most of time expired notification: console.log(e) */)
                        .then(r => r /* Result 201/401 console.log(r) */ );
                }
            }
        }
    }

    return Promise.resolve(user);
}

/* RPC Procedures
*  form: ("procedure", "attribute{d}type"), where {d} is config.delimiter
* */
function completeProcedure(req, e, id, p) {
    try {
        if (scheme[e].p.has(p)) {
            const [a, type] = p.split(config.delimiter);

            switch (type) {
                case "login":
                    return isAuthorized(req, e);

                case "decrement":
                    return getValue(a, 0)
                        .then(v => isInt(type, v))
                        .then(v => setValue(a, v - 1))
                        .then(getEntity);

                case "increment":
                    return getValue(a, 0)
                        .then(v => isInt(type, v))
                        .then(v => setValue(a, v + 1))
                        .then(getEntity);

                case "unsubscribe": {
                    const p = router.db.get(a).getById(id).value();
                    if (req.body.keys.auth !== undefined && p !== undefined) {
                        router.db.get(e).remove({
                            "keys": {"auth": req.body.keys.auth},
                            "projectId": p.id
                        }).write();
                        return Promise.resolve([]);
                    } else {
                        return Promise.reject(Error("Can't subscribed. Maybe the project is removed"));
                    }
                }

                case "subscribe": {
                    const p = router.db.get(a).getById(id).value();

                    if (req.body.keys.auth !== undefined && p !== undefined) {
                        const r = router.db.get(e).filter({
                            "keys": {"auth": req.body.keys.auth},
                            "projectId": p.id
                        }).value();

                        if (r.length === 0) {
                            router.db.get(e).insert(req.body).write();
                            return Promise.resolve([]);
                        } else {
                            return Promise.reject(Error("Already subscribed"));
                        }
                    }
                }

            }
        } else {
            return Promise.reject(Error("Procedure: " + p + " doesn't exists"));
        }
    } catch (e) {
        return Promise.reject(Error(e.toString()));
    }


    function getEntity() {
        return router.db.get(e).getById(id).value();
    }

    function getValue(a, d) {
        try {
            const e = getEntity();
            if (e[a] === undefined) {
                return Promise.resolve(d);
            } else {
                return Promise.resolve(e[a]);
            }
        } catch (e) {
            return Promise.reject(Error(e + " no exists"));
        }
    }

    function setValue(a, v) {
        try {
            const u = {};
            u[a] = v;
            router.db.get(e).getById(id).assign(u).write();

            return Promise.resolve(u);
        } catch (e) {
            return Promise.reject(Error(e + " no exists"));
        }
    }
}


/*
    @types: int, string, bool
    @specifics: posix (posixOnCreate), email, url, fk, fks, hash, base64, bankAcount, bankCode, vat
    @specials: ignore (not write), * (write anything)
*/

/* RULES */

/* f is a function for evaluation
*  e is an expression
*  r are rules
*  p are procedures
* */
function makeRules(json) {
    let result = {};
    for (const entity in json) {
        result[entity] = {};
        result[entity].f = {};
        result[entity].r = {};
        result[entity].p = new Set();
        for (const record in json[entity]) {
            const a = json[entity][record][0];
            const e = json[entity][record][1];
            if (a === "procedure") {
                result[entity].p.add(e);
            } else {
                result[entity].f[a] = rule(e);
                result[entity].r[a] = e;
            }
        }
    }
    return result;
}


function rule(exp) {

    switch (exp) {
        case "ignore":
            return () => Promise.resolve();

        case "*":
            return (t, p) => Promise.resolve(p);

        case "int":
            return (t, p) => isInt(t, p);
        case "int" + config.delimiter + "fk":
            return (t, p, e) => isInt(t, p).then(p => isFk(t, p, e));
        case "int" + config.delimiter + "fks":
            return (t, p, e) => isInt(t, p).then(p => isFk(t, p, e));
        case "int" + config.delimiter + "posix":
            return (t, p) => isInt(t, p).then(p => isPosix(t, p));
        case "int" + config.delimiter + "posixOnCreate":
            return (t, p) => createPosix();

        case "string":
            return (t, p) => isString(t, p).then(p => isShort(t, p));
        case "string" + config.delimiter + "base64":
            return (t, p) => isString(t, p).then(p => isShort(t, p)).then(p => isBase64(t, p));
        case "string" + config.delimiter + "bankCode":
            return (t, p) => isString(t, p).then(p => isShort(t, p)).then(p => isBankCode(t, p));
        case "string" + config.delimiter + "bankAccount":
            return (t, p) => isString(t, p).then(p => isShort(t, p)).then(p => isBankAccount(t, p));
        case "string" + config.delimiter + "email":
            return (t, p) => isString(t, p).then(p => isShort(t, p)).then(p => isEmail(t, p));
        case "string" + config.delimiter + "emailUnique":
            return (t, p, e) => isString(t, p).then(p => isShort(t, p)).then(p => isEmail(t, p)).then(p => isUnique(t, p, e));
        case "string" + config.delimiter + "hash":
            return (t, p) => isString(t, p).then(p => isShort(t, p)).then(p => createHash(t, p));
        case "string" + config.delimiter + "long":
            return (t, p) => isString(t, p).then(p => isLong(t, p));
        case "string" + config.delimiter + "vat":
            return (t, p) => isString(t, p).then(p => isShort(t, p)).then(p => isVat(t, p));
        case "string" + config.delimiter + "vatUnique":
            return (t, p, e) => isString(t, p).then(p => isShort(t, p)).then(p => isVat(t, p)).then(p => isUnique(t, p, e));
        case "string" + config.delimiter + "url":
            return (t, p) => isString(t, p).then(p => isShort(t, p)).then(p => isUrl(t, p));

        case "bool":
            return (t, p) => isBool(t, p);

    }
}

/* INT Rules */
function isInt(t, p) {
    return typeof p === "number" ? Promise.resolve(p) : Promise.reject(Error(t + " není int"));
}

function isFk(t, p, e) {
    return router.db.get(t.replace("Id", "")).getById(p).value() !== undefined ? Promise.resolve(p) : Promise.reject(Error(t + " není fk"));
}

function isFks(t, p, e) {
    // if (Array.isArray()){
    //     for ()
    // }

    return router.db.get(t.replace("Id", "")).getById(p).value() !== undefined ? Promise.resolve(p) : Promise.reject(Error(t + " není fk"));
}

function isPosix(t, p) {
    return p > 0 ? Promise.resolve(p) : Promise.reject(Error(t + " není posix"));
}


/* STRING Rules */
function isShort(t, p) {
    const np = p.trim();
    return np.length === 0 ? Promise.reject(Error(t + " je prázdný")) : np.length < config.string.short ? Promise.resolve(np) : Promise.reject(Error(t + " je příliš dlouhý"));
}

function isLong(t, p) {
    const np = p.trim();
    return np.length === 0 ? Promise.reject(Error(t + " je prázdný")) : np.length < config.string.long ? Promise.resolve(np) : Promise.reject(Error(t + " je příliš dlouhý"));
}

function isString(t, p) {
    return typeof p == "string"? Promise.resolve(p) : Promise.reject(Error(t + " není string"));
}


function isBase64(t, p) {
    return p.match(/^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)?$/) ? Promise.resolve(p) : Promise.reject(Error(t + " není base64"));
}

function isBankCode(t, p) {
    return p.length === 4 ? Promise.resolve(p) : Promise.reject(Error(t + " není číslo banky"));
}

function isBankAccount(t, p) {
    return 4 < p.length < 17 ? Promise.resolve(p) : Promise.reject(Error(t + " není bankovní účet"));
}

function isEmail(t, p) {
    return p.match(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/) ? Promise.resolve(p) : Promise.reject(Error(t + " není email"));
}

function isVat(t, p) {
    return p.length === 8 ? Promise.resolve(p) : Promise.reject(Error(t + " není ičo"));
}

function isUrl(t, p) {
    return p.match(/[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/) ? Promise.resolve(p) : Promise.reject(Error(t + " není url"));
}

/* BOOL rules */
function isBool(t, p) {
    return typeof p === "boolean" ? Promise.resolve(p) : Promise.reject(Error(t + " není bool"));
}

/* UNIVERSAL rules */
function isUnique(t, p, e) {
    const f = {};
    f[t.replace("Unique", "")] = p;
    return router.db.get(e).filter(f).value().length === 0 ? Promise.resolve(p) : Promise.reject(Error(t + " již je v databázi"));
}

/*  Transformers */
function createHash(t, p) {
    try {
        return Promise.resolve(crypto.createHmac('sha256', '94_salt').update(p).digest("base64"));
    } catch (e) {
        return Promise.reject(Error(t + e.toString()))
    }
}

function createPosix() {
    return Promise.resolve(posix());
}

function posix() {
    return Math.floor(new Date())
}

/* type alias Error = { text : Maybe String, code: Maybe Int } */
function Error(t, c) {
    return {t: t, c: c,}
}


/* Make Keys for Web Push */
function getKeys(p) {
    let keys;
    try {
        keys = require(p + ".json");
    } catch (e) {
        keys = wp.generateVAPIDKeys();

        fs.writeFileSync(p + ".json", JSON.stringify(keys));
        fs.writeFileSync("./../fe/" + p + ".js", "const publicVapidKey=" + JSON.stringify(keys.publicKey));
    }

    return keys;
}