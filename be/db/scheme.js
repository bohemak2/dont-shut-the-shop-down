// Generate scheme from FE Entities
const entities = require("../utils/Entities");
const fs = require("fs");

const app = entities.Elm.Entities.init();

app.ports.make && app.ports.make.subscribe(function(result) {
   const db = require("../db/db.json");

   const scheme = db;

   for (const entity in scheme) {
      if (!(entity in result)) {
         delete scheme[entity];
      }
   }

   for (const entity in result){
      if (scheme[entity] === undefined){
         scheme[entity] = [];
      }
   }

   fs.writeFileSync("../db/scheme.json", JSON.stringify(result));
   fs.writeFileSync("../db/db.json", JSON.stringify(scheme));

   console.log("Scheme and DB Synchronized!");
});