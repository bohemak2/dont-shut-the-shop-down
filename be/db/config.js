// Generate scheme from FE Entities
const entities = require("../utils/Config");
const fs = require("fs");

const app = entities.Elm.Config.init();

app.ports.make && app.ports.make.subscribe(function(result) {
   fs.writeFileSync("../db/config.json", JSON.stringify(result));

   console.log("Config Synchronized!");
});