port module Entities exposing (main, make)

import Entity.Message
import Entity.Notification
import Entity.Owner
import Entity.Project

type alias Entity = List (String, String)

main =
    Platform.worker
        { init = \() -> ( (), make encode )
        , update = \_ x -> ( x, Cmd.none )
        , subscriptions = \_ -> Sub.none
        }

port make :
    { message: Entity
    , notification: Entity
    , owner: Entity
    , project: Entity
    }
    -> Cmd msg


encode =
    { message = Entity.Message.db
    , notification = Entity.Notification.db
    , owner = Entity.Owner.db
    , project = Entity.Project.db
    }