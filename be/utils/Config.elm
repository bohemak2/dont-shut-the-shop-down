port module Config exposing (main, make)

import Api.Interface
import Api.Call.Procedure
import Api.Call.Rest
import Entity
import Entity.Owner
import Entity.Project
import Entity.Notification

type alias Config =
    { cascading : List String
    , protect: List String
    , delimiter : String
    , notifications: String
    , prefix : String
    , rest : String
    , rpc : String
    , string: { short : Int, long : Int}
    , version : String
    , users: String
    }

main =
    Platform.worker
        { init = \() -> ( (), make encode)
        , update = \_ x -> ( x, Cmd.none )
        , subscriptions = \_ -> Sub.none
        }

port make : Config -> Cmd msg


encode =
    { cascading = [ Entity.Owner.path,  Entity.Project.path]
    , protect = [ Entity.Notification.path ]
    , delimiter = Entity.delimiter
    , notifications = Entity.Notification.path
    , prefix = Api.Interface.prefix
    , rest = Api.Call.Rest.path
    , rpc = Api.Call.Procedure.path
    , string = Api.Interface.string
    , version = Api.Interface.version
    , users = Entity.Owner.path
    }